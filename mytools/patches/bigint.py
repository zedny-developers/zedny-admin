__author__ = 'spide'

from django.core import exceptions
from django.db.models import fields
from django.utils.translation import ugettext as _


class BigUnsignedIntegerField(fields.IntegerField):
    def db_type(self, connection):  # pylint: disable=W0621
        if 'mysql' in connection.__class__.__module__:
            return 'bigint unsigned'
        return super(BigUnsignedIntegerField, self).db_type(connection)


    def get_internal_type(self):
        return "BigUnsignedIntegerField"

    def to_python(self, value):
        if value is None:
            return value
        try:
            return float(value)
        except (TypeError, ValueError):
            raise exceptions.ValidationError(
                _("This value must be a long integer."))


class BigAutoField(fields.AutoField):
    def db_type(self, connection):  # pylint: disable=W0621
        if 'mysql' in connection.__class__.__module__:
            return 'bigint UNSIGNED AUTO_INCREMENT'
        return super(BigAutoField, self).db_type(connection)


    def get_internal_type(self):
        return "BigAutoField"

    def to_python(self, value):
        if value is None:
            return value
        try:
            return float(value)
        except (TypeError, ValueError):
            raise exceptions.ValidationError(
                _("This value must be a long integer."))


class BigForeignKey(fields.related.ForeignKey):
    def db_type(self, connection):
        rel_field = self.rel.get_related_field()
        # next lines are the "bad tooth" in the original code:
        if (isinstance(rel_field, BigAutoField) or
                (not connection.features.related_fields_match_type and
                     isinstance(rel_field, BigUnsignedIntegerField))):
            # because it continues here in the django code:
            # return IntegerField().db_type()
            # thereby fixing any AutoField as IntegerField
            return BigUnsignedIntegerField().db_type(connection)
        return rel_field.db_type(connection)
