from rest_framework.views import exception_handler

from backend.models import User, UserRole
from erp import parameters
from erp.parameters import ERROR_CODE_EMPTY, ERROR_CODE_REQUIRED, ERROR_INVALID_INPUT
from rest_framework.response import Response


def CheckPrivileges(req, target_permission):
    email = req.user
    try:
        user = User.objects.get(email=email)
        if not user.is_admin:
            print('User not admin')
            # Check if permission required admin
            if target_permission in parameters.USER_PERMISSIONS:
                return user;
            else:
                return False

        if (user.is_admin or user.accountType == parameters.MEMBER_MASTER
            or user.accountType == parameters.MEMBER_ADMIN):
            return user

        if user.accountType == parameters.MEMBER_STAFF:
            permission = UserRole.objects.get(user=user, role__name=target_permission)
            print(permission)
            if (permission):
                return user
    except User.DoesNotExist:
        print('User not exist');
        return False;
    except UserRole.DoesNotExist:
        print('Permission  not exist');
        return False;

    return False


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['status_code'] = response.status_code

    return response


def return_integrity_error(key, msg, details=None, code=None):
    if code is not None:
        errors_details = {key: {
            'message': msg,
            'code': code
        }}
    else:
        errors_details = {key: {
            'message': msg,
            'code': parameters.ERROR_CODE_DUPLICATE
        }}

    if parameters.DEBUG and details != None:
        errors_details[key]["details"] = details

    validation = {"Errors": errors_details,
                  "ErrorNumber": parameters.ERROR_UNIQUE_VIOLATION}

    return Response(validation, status=400)  # 406 not acceptable


def return_generic_error(msg, error_number=parameters.ERROR_INTERNAL_ERROR,
                         code=parameters.ERROR_CODE_PROCESSING_ERROR, details=None, status=500):
    errors_details = {'message': msg,
                      'code': code}
    if parameters.DEBUG and details != None:
        errors_details["details"] = details

    validation = {"Error": errors_details,
                  "ErrorNumber": error_number}

    return Response(validation, status=status)  # 406 not acceptable


def return_field_error(key, msg, error_number=parameters.ERROR_INTERNAL_ERROR,
                       code=parameters.ERROR_CODE_PROCESSING_ERROR, details=None):
    errors_details = {key: {'message': msg,
                            'code': code}}
    if parameters.DEBUG and details != None:
        errors_details[key]["details"] = details

    validation = {"Errors": errors_details,
                  "ErrorNumber": error_number}

    return Response(validation, status=400)  # 406 not acceptable


def checkRequestParameters(request, parameters):
    validation = {}
    errors = {}
    for para in parameters:
        if para in request:
            val = request.get(para)
            if not val.strip():
                errors[para] = {'message': 'field can not be empty', 'code': ERROR_CODE_EMPTY}
                #                 for k, v in request.data.iteritems():
                #                     print k;
                #                     print v;
                #                     print '-------'
        else:
            errors[para] = {'message': 'this field is missing', 'code': ERROR_CODE_REQUIRED}

    if len(errors) > 0:
        validation['Errors'] = errors
        validation['ErrorNumber'] = ERROR_INVALID_INPUT

    return validation


def getPathFromMime(mime):
    if "video" in mime:
        return "videos"
    elif "image" in mime:
        return "images"
    else:
        return "files"


def getMimeType(my_file):
    # Linux only
    # mime = magic.from_file(my_file)
    mime = my_file.content_type
    return mime


def getMediaType(mime):
    if "video" in mime:
        return parameters.MediaType_VIDEO
    elif "image" in mime:
        return parameters.MediaType_IMAGE
    else:
        return parameters.MediaType_DOCUMENT


def is_number(s):
    try:
        float(s)
        return True
    except TypeError:
        return False
    except ValueError:
        return False
