from db.models import Role, Permissions, UserRole


class Auth:
    @staticmethod
    def can_execute(user, task):
        parts = task.split(':')
        rolesWithTask = Permissions.objects \
            .values_list('role__id', flat=True) \
            .filter(action=parts[1], target=parts[0])
        rolesWithTask = list(rolesWithTask)
        # print(rolesWithTask)
        userHasAccessedRole = UserRole.objects.filter(user=user,
                                                      role_id__in=rolesWithTask).exists()
        if userHasAccessedRole:
            return {
                'access': True,
                'msg': "success"
            }
        else:
            return {
                'access': False,
                'msg': "Access denied."
            }

    @staticmethod
    def check_client(user, client):
        pass

    @staticmethod
    def has_role(user, role):
        return True

    @staticmethod
    def has_permission(user, permission):
        return True

    @staticmethod
    def role_has_permissions(role, permissions):
        return {}

    @staticmethod
    def is_user_disabled(user):
        pass
