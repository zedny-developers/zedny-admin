Vue.component('date-picker', {
    template: `<input @input="$emit('input', $event.target.value)" :value="date" v-date-picker/>`,
    props: {
        date: {
            default: '',
            type: String,
        }
    },
    directives: {
        datePicker: {
            bind(el, binding, vnode) {
                $(el).datepicker({
                    onSelect: function(val) {
                        vnode.context.$emit('input', val);
                    }
                });
            }
        }
    }
});
