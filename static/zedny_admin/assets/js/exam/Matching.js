
var create_counter_matching = 0  ;
var quest_edit_id_now  = 0  ;
var edit_quest_counter = 0 ;

function createNewMatchQuest( tag , context) {
    var template = $("#Matching-template").html();
    var compile = Handlebars.compile(template);
    $("#"+tag).append(compile(context));

}

function addCreateChoiceMatching(){
    create_counter_matching++;
    createNewMatchQuest("choices_create_matching" , {id : create_counter_matching , "quest_id" : 0});
}

function addMatchingEditChoice(){
    edit_quest_counter++;
    console.log(quest_edit_id_now)
    createNewMatchQuest('choices_edit' , { id: edit_quest_counter , answer: '' , match_answer : '', "quest_id" : quest_edit_id_now} );
}

$('.edit-matching-quest').on('click',function(e) {
     $('#choices_edit').empty();
    var quest_id = $(this).data('id');
    quest_edit_id_now = quest_id ;
    $('#quest_edit_area').val($('#txt_quest_'+quest_id).val())
      var count = parseInt($('#ans_info_'+quest_id).data('id')) ;
      edit_quest_counter = count ;
     for (var i = 1 ; i <= count ; i++){
        var quest = $('#ans_quest_'+quest_id+'_'+i).val();
        var match_ans = $('#match_answer_'+quest_id+'_'+i).text();

        createNewMatchQuest( 'choices_edit' , { id: i , answer: quest , match_answer : match_ans , "quest_id" : quest_id });
     }
});

