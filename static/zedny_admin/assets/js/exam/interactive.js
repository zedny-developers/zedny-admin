    var testCasesData = {};
    var line_counter = 0  ;
    var element_counter = 0 ;

    //--------------------------- Line
    function CreateLine(tag , context) {
        var template = $("#testcase-template").html();
        var compile = Handlebars.compile(template);
        $("#"+tag).append(compile(context));

    }
    function addLine(){
        $("#no_lines").addClass("hide");
        line_counter++;
        CreateLine("test_cases_lines" , {"id" : line_counter });
    }

    function deleteLine(id){
           $("#line_"+id).children().each(function(e){
             if (testCasesData[$(this).html()] != null){
                delete testCasesData[$(this).html()];
                 }
            });


            if(Object.keys(testCasesData).length === 0) {
                $("#no_lines").removeClass("hide");
            }

        $("#line_block_"+id).remove();
        $("#line_num_text_"+id).remove();
        $("#test_templete").val(JSON.stringify(testCasesData));
    }

    //------------------------------ Element
    function addElementTemplete( line_id , element_id){
    return '<a class="bold col-md-1" id = "element_'+line_id+'_'+element_id+'" style="font-size:17px; color:#2C7289; cursor: pointer;" onclick = "fetch_data_to_field('+element_id+')">E'+element_id+'</a>'
    }

    function addElement(line_id){
        element_counter++;
        $("#line_"+line_id).append(addElementTemplete(line_id ,element_counter ));
    }
    function deleteElement(line_id){
         var element = $("#line_"+line_id).children().last().html() ;
         if (testCasesData[element] != null){
            delete testCasesData[element];
            }
        $("#line_"+line_id).children().last().remove();
        $("#test_templete").val(JSON.stringify(testCasesData));
    }

    // empty fields
    function empty_fields(){
        $("#min_length").val("");
        $("#max_length").val("");
    }


    // fetch data to fields
    function fetch_data_to_field(element_id){
    $("#linked_div").addClass("hide");
    empty_fields();
    // get title of element
    $("#element_title").html("E"+element_id);
    // hide save label
    $("#save_data").addClass("hide")
    $("#save_data").parent().addClass("hide");

    var obj = testCasesData["E"+element_id] ;
        if (obj != null){
             // fetch data
            $("#type").val(obj["value"]);
            $("#min_length").val(obj["min_length"]);
            $("#max_length").val(obj["max_length"]);
            // fetch linked with array field
            if (obj["value"] == $("#linked_array_val").html() ){
                $("#type_no_linked").val(obj["type_no_linked"]);
                $("#element_list").val(obj["element_linked"]);
                    $("#linked_div").removeClass("hide");
            }

        }
    }

    // save data to dictionary
    function SaveElementObject(){

        var element = $("#element_title").html();
            $("#save_data").removeClass("hide");
            $("#save_data").parent().removeClass("hide");

         if ( $("#min_length").val() === '' ||("#max_length").val === ''){
             $("#save_data").html("fill empty fields")
             $("#save_data").removeClass("text-success");
             $("#save_data").addClass("text-danger");
             return ;
         }
             $("#save_data").html("Saved !!")
             $("#save_data").addClass("text-success");
             $("#save_data").removeClass("text-danger");

        var type_no_linked = "null" ,  element_list = "null";
        if ( $("#type").val() == $("#linked_array_val").html()){
         type_no_linked  = $("#type_no_linked").val();
         element_list = $("#element_list").val();
        }

        testCasesData[element] = {"value" : $("#type").val() , "min_length" : $("#min_length").val()  , "max_length" : $("#max_length").val()  ,  "type_no_linked" :  type_no_linked , "element_linked" : element_list}

        $("#test_templete").val(JSON.stringify(testCasesData));
        console.log(JSON.stringify(testCasesData));
    }





    // hide tools for each type
    function hideTools(){
       $("#linked_div").addClass("hide");
    }


    $('#type').change(function(){
        if ($(this).val() == $("#linked_array_val").html()){
            // remove hide
             $("#linked_div").removeClass("hide");
            // delete all options
            $("#element_list").find('option').remove().end()
            // add all elements that saved
            for (var key in testCasesData) {
              $("#element_list").append('<option value="'+key+'">'+key+'</option>');
            }
        }else{
            $("#linked_div").addClass("hide");
        }
    });




