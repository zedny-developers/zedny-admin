
// -------------------- create -------------------

var quest_edit_id_now  = 0  ;

var create_quest_counter = 0  , edit_quest_counter = 0 ;

function createNewQuest( tag , context) {
    var template = $("#MCQ-template").html();
    var compile = Handlebars.compile(template);
    $("#"+tag).append(compile(context));

}

function addCreateChoiceMCQ(){
    create_quest_counter++;
    createNewQuest('choices_create_MCQ' , { id: create_quest_counter , answer: '' , score : '' , is_correct : false, "quest_id" : "0"} );
}

//------------------------------------

function removeAns( ans_id , quest_id ){
 $('#remove_ans_' + ans_id+'_'+quest_id).parent().empty();
}

function ToggleDegree(id){
    if ( $("#degree_" + id).hasClass("hide")){
        $("#degree_" + id).removeClass("hide");
    }else {
        $("#degree_" + id).addClass("hide");
    }
       $("#degree_" + id).val("");
}
//--------------------------------------------------------------

function addMCQEditChoice(){
    edit_quest_counter++;
    createNewQuest('choices_edit' , { id: edit_quest_counter , answer: '' , score : '' , is_correct : false , "quest_id" : quest_edit_id_now} );
}

$('.edit-MCQ-quest').on('click',function(e) {
    $('#choices_edit').empty();
    var quest_id = $(this).data('id');
    quest_edit_id_now = quest_id ;
    $('#quest_edit_area').val($('#txt_quest_'+quest_id).val())
      var count = parseInt($('#ans_info_'+quest_id).data('id')) ;
      edit_quest_counter = count ;
     for (var i = 1 ; i <= count ; i++){
        var value = parseInt($('#score_quest_'+quest_id+'_'+i).val()) ;
        createNewQuest( 'choices_edit' , { id: i , answer: $('#ans_quest_'+quest_id+'_'+i).val() , score : $('#score_quest_'+quest_id+'_'+i).val() , is_correct : value != 0  , "quest_id" : quest_id });
        }
});

