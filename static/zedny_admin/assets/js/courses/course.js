var adjustment,oldContainer;

const filePrev = Vue.extend({
    template: '#filePreviewer-template',
    props : ['fileprevdata'],
    data: function () {
        return {
            file: '',
            showPreview: false,
            src: this.$props.fileprevdata.src,
            hasError: ''
        }
    },
    created: function () {
        if (this.src){
            this.showPreview = true;
        }
        if (this.$props.fileprevdata.errors) {
            this.hasError = this.$props.fileprevdata.errors;
        }
    },
    methods: {
        loadFile: function() {
            let reader  = new FileReader();
            this.file = this.$refs.file.files[0]
            reader.addEventListener("load", function () {
                this.showPreview = true;
                this.src = reader.result;
            }.bind(this), false);
            reader.readAsDataURL( this.file );
        },
        removedata: function () {
            this.file = '';
            this.showPreview = '';
            this.src = '';
        },
    }
});

Vue.component('modal', {
    template: '#modal-template'
});
Vue.component('date-picker', {
    template: `<input @input="$emit('input', $event)" :id="id"  :name="name" :value="pickedValue" v-date-picker/>`,
    props:
        ['pickedValue', 'name', 'id'],
    directives: {
        datePicker: {
            bind(el, binding, vnode) {
                $(el).datepicker({
                    onSelect: function(val) {
                        vnode.context.$emit('pickedValue', val);
                        console.log(binding.value);
                    }
                });
            }
        }
    }
});

var sortableList = Vue.component('sortable-list',{
    template: '#sortable-list-template',
    props: [
        'data','courseid'
    ],
    data: function () {
        return {
            info: {},
            showModal: false
        }
    },
    watch: {
        showModal: function () {
            app.showModal = this.showModal
        }
    },
    mounted: function() {
        var group = $('ul.draggable-list').sortable({
            group: 'draggable-list',
            pullPlaceholder: false,
            
            // animation on drop
            onDrop: function  ($item, container, _super) {
                _super($item, container);
                var $clonedItem = $('<li/>').css({height: 0});
                $item.before($clonedItem);
                $clonedItem.animate({'height': $item.height()});
            
                $item.animate($clonedItem.position(), function  () {
                    $clonedItem.detach();
                    _super($item, container);
                });
                var data = group.sortable("serialize").get();
                data = data[0].filter(value => Object.keys(value).length !== 0);
                var i = 1;
                data.forEach(element => {
                    element.order = i;
                    i++
                });
                var jsonString = JSON.stringify(data);
                $.post('/courses/ajax',{'csrfmiddlewaretoken':csrfToken,'filter':'updateContentList','list':jsonString,'course_id':course_id})              
            },
          
            // set $item relative to cursor position
            onDragStart: function ($item, container, _super) {
              var offset = $item.offset(),
                  pointer = container.rootGroup.pointer;
          
              adjustment = {
                left: pointer.left - offset.left,
                top: pointer.top - offset.top
              };
          
              _super($item, container);
            },
            onDrag: function ($item, position) {
              $item.css({
                left: position.left - adjustment.left,
                top: position.top - adjustment.top
              });
            }
        });
    }
});
Vue.component('form-wizard',{
    template:   `<div id="rootwizard" class="col-md-12">
                    <div class="form-wizard-steps" style="margin-bottom: 15px;">
                        <ul class="wizard-steps">
                            <li v-bind:class="{'active':step.active}"  style="width: 30%;" v-for="(step, index) in data">
                                <a  v-bind:href="step.target">
                                    <span class="step">{{ index+1 }}</span>
                                    <span class="title">{{ step.title }}</span>
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="tab-content transparent">
                        <div class="tab-pane active" v-bind:id="step.id" v-for="(step, index) in data">
                            <div class="row">
                                <slot :name="step.id"></slot>
                                
                            </div>
                        </div>
                    </div>
                </div>`,
    props: [
        'data'
    ],
    mounted: function(){
        $('#course_offer_end_date').datepicker({
            onSelect:function(selectedDate, datePicker) {            
               
            }
        });
        $('.text-editor').summernote({
            rows: 30
        });
        $('#material_of').select2();
        $('#material_content').select2();
        $('#material_of').on('change',function (e) {
            e.preventDefault();
            app.selectedOf = $('#material_of').val();
        });
        $("#material_file").bind('change',function(){
            $("#material_size").val(this.files[0].size);
        });
        // $('#content_type').select2();
        $('#content_cover').change(function(){
            
        })
        $('#content_type').on('change',function(e) {
            e.preventDefault();
            app.contentType = $('#content_type').val();
            if ($('#content_type').val() == 'Video') {
                // 
            } else if ($('#content_type').val() == 'Quiz' || $('#content_type').val() == 'Exam'){
                getContentTypeData($('#content_type').val());
            }
        })
    }
});


function getContentTypeData(filter) {
    $.get('/courses/ajax', {filter:filter},function( data ){
        $('#content_type_id').select2({
            data: data
        });
        console.log(data);
    })
}

Vue.component('cat-item', {
    template: '#category-item-template',
    data: function () {
        return {
            categories: this.$props.data,
            open: false,
        }
    },
    props: {
        data: Array,
        selectCategory: Function,
        toggleCategoryDelete: Function,
        toggleCategoryForm: Function
    },
    methods: {
        toggle: function (arg) {
            var self = this
            if (arg.children) {
                this.open = !this.open;
            } else {
                $.get('/categories/ajax',{filter:'children',id:arg.id}, function (data) {
                    var found = self.$props.data.find(x => x.id == arg.id)
                    found.children = data;
                    self.open = !self.open;
                });
            }
        },
    }
})

const metaData = Vue.extend({
    template: '#category-tag-template',
    data: function () {
        return {
            active:"cat",
            categories: '',
            actionId: null,
            submit: '',
            search: '',
            formToggle: false,
            deleteToggle: false,
            categoryList:'',
            tagsList: [],
            selectedCats: [],
            selectIndex: 0,
            prevSelectIndex: 0,
        }
    },
    attached: function() {
        $(this.$el).find('#tagsinput').tagsinput();
    },
    computed: {
        selectedTags: {
            get: function () {
                app.tags = $(this.$el).find('#tagsinput').val();
                return $(this.$el).find('#tagsinput').val();
            }
        },
        hasChildren: function () {
            return this.categories.hasChildren
        },
        filteredList: function() {
            return this.categoryList.filter(category => {
              return category.name.toLowerCase().includes(this.search.toLowerCase())
            })
        }
    },
    watch:{
        deleteToggle: function() {
            if (!this.deleteToggle) {
                this.actionId = null;
            }
        },
        selectedCats: function () {
            app.cats = this.selectedCats;  
        }
    },
    mounted: function () {
        var self = this;
        $.get('/categories/ajax',{filter:'parents'}, function (data) {
            self.categories = data
        });
        $.get('/categories/ajax',{filter:'all'}, function (data) {
            self.categoryList = data
        });
        $.get('/tags/ajax',{filter:'all'}, function (data) {
            var tags = $.map(data, function (tag) {
                return {id: tag.id, name: tag.name}
            })
            self.tagsList = tags
        });
        $('#tagsinput').tagsinput();
        $('#tagsinput').tagsinput('input').on('keypress', function (event) {
            $.get('/tags/ajax',{filter:'search',searchKey:event.target.value}, function (data) {
                var tags = $.map(data, function (tag) {
                    return {id: tag.id, name: tag.name}
                })
                self.tagsList = tags
            });
        });
        $('#tagsinput').tagsinput('input').on('keydown', function (event) {
            var key = event.keyCode || event.charCode;
            
            if( key == 8 || key == 46 ) {
                var searchKey = $('#tagsinput').tagsinput('input').val().slice(0, -1);
                $.get('/tags/ajax',{filter:'search',searchKey:searchKey}, function (data) {
                    var tags = $.map(data, function (tag) {
                        return {id: tag.id, name: tag.name}
                    })
                    self.tagsList = tags
                });
            }
            if (key == 38) { //up
                self.selectIndex = self.selectIndex -1
                if (self.selectIndex == -1) 
                    self.selectIndex = self.tagsList.length -1
                $('#tagsinput').tagsinput('input').val(self.tagsList[self.selectIndex].name)
                $("#tagList>li.selected-tag").removeClass("selected-tag");
                $('#tag' + self.tagsList[self.selectIndex].id + self.tagsList[self.selectIndex].name ).addClass('selected-tag') 
            }
            if (key == 40) { //down
                self.selectIndex = self.selectIndex + 1
                if (self.selectIndex == self.tagsList.length) 
                    self.selectIndex = 0
                $('#tagsinput').tagsinput('input').val(self.tagsList[self.selectIndex].name)
                $("#tagList>li.selected-tag").removeClass("selected-tag")
                $('#tag'+self.tagsList[self.selectIndex].id+self.tagsList[self.selectIndex].name).addClass('selected-tag')
            }
        });
        $('#tagsinput').on('itemAdded',function (event) {
            var tag = self.tagsList.filter(tag => {
                return tag.name.toLowerCase().includes(event.item.toLowerCase())
              })
            if (tag.length == 0) {
                $.post('/tags/create',{'csrfmiddlewaretoken':csrfToken,'filter':'ajaxCreate',name:event.item,id:event.target.value},function (data) {
                    var tags = $.map(data, function (tag) {
                        return {id: tag.id, name: tag.name}
                    })
                    self.tagsList = tags
                });     
            }
            self.selectIndex = 0
        });
        $('#tagsinput').on('change',function (e) {
            app.tags = $('#tagsinput').val();
            this.selectedTags = $('#tagsinput').val();
        })
        $('#select-parent').select2({
            placeholder: 'Select Parent',
            data: self.categoryList
        });
    },
    methods: {
        setActive: function (arg) {
            this.active = arg
        },
        selectCategory: function (arg,event) {
            if (event.target.checked){
                this.selectedCats.push(arg.name)
            } else {
                this.selectedCats.pop(arg.name)
            }
        },
        toggleAddCategory: function () {
            this.submit = 'create';
            if (this.actionId) {
                this.actionId = null
                $('#add-category-name').val('');
                $('#select-parent').select2('val', '0');
            } else {
                this.formToggle = !this.formToggle;
                $('#add-category-name').val('');
                $('#select-parent').select2('val', '0');
            }
        },
        toggleCategoryForm: function (arg) {
            this.submit = 'edit';
            this.$emit('editCat', arg);
            if (!this.formToggle) {
                this.formToggle = true;
                this.actionId = arg.id;
                $('#select-parent').val(arg.parent).trigger('change');
                $('#add-category-name').val(arg.name);
            } else {
                if ($('#add-category-name').val() && this.actionId != arg.id) {
                    this.actionId = arg.id;
                    $('#add-category-name').val(arg.name);
                    $('#select-parent').val(arg.parent).trigger('change');
                } else {
                    this.formToggle = false
                    this.actionId = null
                    $('#add-category-name').val('');
                    $('#select-parent').select2('val', '0');
                }
            }
        },
        toggleCategoryDelete: function (arg) {
            this.$emit('delCat', arg);
            this.deleteToggle = true
            this.actionId = arg
        },
        submitCategory: function (params) {
            if (this.submit == 'create') {
                this.addCategory()
            } else if (this.submit == 'edit') {
                this.editCategory()
            }
        },
        addCategory: function () {
            var self = this;
            name = $('#add-category-name').val();
            parent = $('#select-parent').val();
            if(name !== ''){
                $.post('/categories/create',{'csrfmiddlewaretoken':csrfToken,'filter':'ajaxCreate',name:name,parent:parent},function (data) {
                    self.categories = data;
                    $('#add-category-name').val('');
                    $('#select-parent').select2('val', '0');
                    $.get('/categories/ajax',{filter:'all'}, function (data) {
                        self.categoryList = data
                    });
                });
            }
        },
        deleteCategory: function () {
            var self = this, arg = this.actionId
            $.ajax({
                type: 'DELETE',
                url: '/categories/destroy/' + arg,
                data: { 'id': arg },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("X-CSRFToken", csrfToken);
                },
                success: function (data) {
                    self.categories = data;
                    $.get('/categories/ajax',{filter:'all'}, function (data) {
                        self.categoryList = data
                    });
                }
            })
        },
        editCategory: function () {
            var self = this;
            name = $('#add-category-name').val()
            parent = $('#select-parent').val()
            $.ajax({
                type: 'PUT',
                url: '/categories/edit/' + this.actionId,
                data: { filter: 'ajaxUpdate', 'id': this.actionId, name: name, parent: parent},
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("X-CSRFToken", csrfToken);
                },
                success: function (data) {
                    self.categories = data;
                }
            })
            $.get('/categories/ajax',{filter:'all'}, function (data) {
                self.categoryList = data
            });
        }
    },
});

var app = new Vue({
    el:'#vue-app',
    data: {
        hasOffer: '',
        contentType: '',
        selectedOf: '',
        toggleDeleteModal: false,
        showModal: false,
        fileprevdata: null,
        tags: [],
        cats: []
    },
    watch: {
        showModal: function () {
            sortableList.showModal = this.showModal
            console.log(sortableList.showModal);
        }
    },
    methods: {
        miniWindow: function (target) {
            window.open(target, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400")    
        },
    },
    components: {
        filePrev, metaData
    }
});

app.contentType = contentType

