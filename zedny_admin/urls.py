from django.urls import path

from zedny_admin.views.account import admin_login, zedny_logout
from zedny_admin.views.acl import roles_index, roles_create, roles_update, role_delete, userroles_index, \
    user_roles_create, permissions_index, permission_create
from zedny_admin.views.book_summary import book_index, book_create, book_update, book_delete, book_summary_create
from zedny_admin.views.categoriesAndTags import CategoryResourceView, TagResourceView
from zedny_admin.views.courses import tracking, courses_index, CourseCUDView, CourseDetailsView, \
    CreateContentView, AJAX_Data, ContentDetailsView, ContentDeleteView, CreateMatrialView, MaterialDownloadView, \
    MatrialDeleteView
from zedny_admin.views.exams import exams_index, exam_create, CreateQuestion, exam_delete, DeleteQuestions, \
    UpdateQuestions, CreateInteractiveQuestion, CreateTestCaseTemplete
from zedny_admin.views.home import index
from zedny_admin.views.inbox import inbox
from zedny_admin.views.messges import messages
from zedny_admin.views.users import user_index, user_create, user_update, user_delete

__author__ = 'mahmoud'

from django.conf.urls import url

# Routers provide an easy way of automatically determining the URL conf.
# router = routers.DefaultRouter()
# router.register(r'profile', views.UserProfileViewSet)

# profiles_list = UserProfileViewSet.as_view({
#     'get': 'list'
# })

urlpatterns = [
    url(r'^$', index, name='admin-index-empty'),
    url(r'^login', admin_login, name='admin-login'),
    url(r'^logout', zedny_logout, name='admin-logout'),
    url(r'^index', index, name='admin-index'),
    # Messages
    url(r'^messages', messages, name='admin-messages'),
    url(r'^inbox', inbox, name='admin-inbox'),
    # Courses
    # url(r'^courses/tracking', tracking, name='admin-courses-tracking'),
    # url(r'^courses/index', courses_index, name='admin-courses-index'),
    # url(r'^courses/create', courses_create, name='admin-courses-create'),

    # ########### Amr ###########
    # Exams
    url(r'^exams/index', exams_index, name='admin-exams-index'),
    url(r'^exams/create', exam_create, name='admin-exams-create'),
    url(r'^exams/questions/edit/(?P<exam_id>[0-9]+)/', CreateQuestion, name='admin-exam-questions-edit'),
    url(r'^exams/exam/delete/(?P<exam_id>[0-9]+)/', exam_delete, name='admin-exam-delete'),
    url(r'^exams/questions/delete/(?P<exam_id>[0-9]+)/(?P<quest_id>[0-9]+)/', DeleteQuestions,
        name='admin-exam-questions-delete'),
    url(r'^exams/questions/update/(?P<exam_id>[0-9]+)/', UpdateQuestions, name='admin-exam-questions-update'),
    # interactive questions
    url(r'^exams/questions/interactive/create/(?P<exam_id>[0-9]+)/', CreateInteractiveQuestion,
        name='admin-exam-question-inter-create'),

    url(r'^exams/questions/interactive/create/test_case_templete/(?P<exam_id>[0-9]+)/', CreateTestCaseTemplete,
        name='admin-exam-question-test-case-create'),

    ##### Ashraf ############
    # Users
    path('users/', user_index, name='admin-users-index'),
    path('users/create', user_create, name='admin-users-create'),
    path('users/<int:id>/update', user_update, name='admin-user-update'),
    path('users/<int:id>/delete', user_delete, name='admin-user-delete'),
    # User Roles
    # Roles
    path('roles/', roles_index, name='admin-roles-index'),
    path('roles/create', roles_create, name='admin-roles-create'),
    path('roles/<int:id>/update', roles_update, name='admin-role-update'),
    path('roles/<int:id>/delete', role_delete, name='admin-role-delete'),
    # User Roles
    path('roles/user-role/', userroles_index, name='admin-user-roles-index'),
    path('roles/user-role/create', user_roles_create, name='admin-user-roles-create'),
    # Permission
    path('roles/permissions/', permissions_index, name='admin-permissions-index'),
    path('roles/permissions/create', permission_create, name='admin-permission-create'),
    # Books
    path('books/', book_index, name='admin-books-index'),
    path('books/create', book_create, name='admin-books-create'),
    path('books/<int:id>/update', book_update, name='admin-book-update'),
    path('books/<int:id>/delete', book_delete, name='admin-book-delete'),
    # Book Summaries
    path('book-summary/', book_index, name='admin-books-index'),
    path('book-summary/<int:id>/create', book_summary_create, name='admin-book-summary-create'),
    path('book-summary/<int:id>/update', book_update, name='admin-book-update'),
    path('book-summary/<int:id>/delete', book_delete, name='admin-book-delete'),

    # ##### SOHYPE ###########
    # categories
    url(r'^categories/', CategoryResourceView.as_view(), name='admin-categories-index'),
    url(r'^categories/create', CategoryResourceView.as_view(), name='admin-categories-create'),
    url(r'^categories/ajax', CategoryResourceView.as_view(), name='admin-categories-ajax'),
    url(r'^categories/edit/(?P<category_id>[0-9]+)', CategoryResourceView.as_view(), name='admin-categories-edit'),
    url(r'^categories/destroy/(?P<category_id>[0-9]+)', CategoryResourceView.as_view(), name='admin-categories-delete'),
    # tags
    url(r'^tags/create', TagResourceView.as_view(), name='admin-tags-create'),
    url(r'^tags/ajax', TagResourceView.as_view(), name='admin-tags-ajax'),
    # Courses
    url(r'^courses/tracking', tracking, name='admin-courses-tracking'),
    url(r'^courses/index', courses_index, name='admin-courses-index'),
    url(r'^courses/create', CourseCUDView.as_view(), name='admin-courses-create'),
    url(r'^courses/(?P<course_id>[0-9]+)', CourseDetailsView.as_view(), name='admin-course-details'),
    url(r'^courses/edit/(?P<course_id>[0-9]+)', CourseCUDView.as_view(), name='admin-course-edit'),
    url(r'^courses/destroy/(?P<course_id>[0-9]+)', CourseCUDView.as_view(), name='admin-course-delete'),
    url(r'^courses/ajax', AJAX_Data.as_view(), name='admin-courses-ajax-data'),
    # contents
    url(r'^contents/create/(?P<course_id>[0-9]+)', CreateContentView.as_view(), name='admin-contents-create'),
    url(r'^contents/edit/(?P<course_id>[0-9]+)/(?P<content_id>[0-9]+)', CreateContentView.as_view(),
        name='admin-contents-edit'),
    url(r'^contents/(?P<content_id>[0-9]+)', ContentDetailsView.as_view(), name='admin-contents-details'),
    url(r'^contents/destroy/(?P<content_id>[0-9]+)', ContentDeleteView.as_view(), name='admin-content-delete'),
    # materials
    url(r'^materials/create/(?P<course_id>[0-9]+)', CreateMatrialView.as_view(), name='admin-materials-create'),
    url(r'^materials/download/(?P<material_id>[0-9]+)', MaterialDownloadView.as_view(),
        name='admin-materials-download'),
    url(r'^materials/destroy/(?P<material_id>[0-9]+)', MatrialDeleteView.as_view(), name='admin-materials-delete'),

    # Errors
    # url(r'errors/invalid-access$', invalid_access, name='admin-invalid-access'),
]
