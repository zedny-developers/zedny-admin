from django.contrib.auth.decorators import login_required
from django.views import View
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from db.models import Courses, Content, Videos, Materials, TranScripts, Subtitles, Paths, Exam
from zedny_admin.forms.courses import *
from django.urls import reverse
from django.db import transaction
from django.core import serializers

@login_required(login_url='admin-login', redirect_field_name='next')
def tracking(request):
    # goTo = check_is_admin(request, show_error_page=False)
    # if goTo is not None:
    #     return redirect(goTo)

    return render(request, 'zedny_admin/courses/tracking.html')


def courses_index(request):
    # goTo = check_is_admin(request, show_error_page=False)
    # if goTo is not None:
    #     return redirect(goTo)

    return render(request, 'zedny_admin/courses/index.html', {
        'courses': Courses.objects.all()
    })

class AJAX_Data(View):
    def get(self, request):
        import json
        if request.GET.get('filter') == 'updateContentList':
            pass
        elif request.GET.get('filter') == 'Exam':
            pass
            pass
        elif request.GET.get('filter') == 'Exam':
            pass
        else:
            return JsonResponse('No data found!')

    def post(self, request):
        import json
        if request.POST.get('filter') == 'updateContentList':
            list =  json.loads(request.POST.get('list'))
            for listItem in list:
                content = Content.objects.get(pk= int(listItem['pk']))
                content.order = int(listItem['order'])
                content.save()
            return JsonResponse('ok', safe=False)
# Course Create, Update And Delete
class CourseCUDView(View):
    def dispatch(self, *args, **kwargs):
        method = self.request.POST.get('_method', '')
        if method == 'PUT':
            return self.put(*args, **kwargs)
        if method == 'DELETE':
            return self.destroy(*args, **kwargs)
        return super(CourseCUDView, self).dispatch(*args, **kwargs)

    def get(self, request, course_id=None):
        if course_id is not None:
            course = CoursesForm(instance=Courses.objects.get(pk=course_id))
        else:
            course = CoursesForm
        return render(request, 'zedny_admin/courses/create.html',{
            'course': course,
            'step': 'course',
            'course_id': course_id
        })
    
    def post(self, request):
        course = CoursesForm(request.POST, request.FILES)
        if course.is_valid():
            courseInstance = course.save(commit=False)
            courseInstance.instructor = User.objects.get(pk=request.POST.get('instructor'))
            courseInstance.save()
            return HttpResponseRedirect(reverse('admin-contents-create', kwargs={'course_id':str(courseInstance.pk)}))
        return render(request, 'zedny_admin/courses/create.html',{
            'course': course,
            'step': 'course',
        })

    def put(self, *args, **kwargs):
        print('here')
        course_id = self.kwargs['course_id']
        print(self.request.FILES)
        course = CoursesForm(self.request.POST, self.request.FILES, instance=Courses.objects.get(pk=course_id))
        if course.is_valid():
            courseInstance = course.save(commit=False)
            courseInstance.instructor = User.objects.get(pk=self.request.POST.get('instructor'))
            courseInstance.save()
            return HttpResponseRedirect(reverse('admin-course-edit', kwargs={'course_id':str(course_id)}))
        return render(self.request, 'zedny_admin/courses/create.html',{
            'course': course,
            'step': 'course',
        })

    def destroy(self, *args, **kwargs):
        course_id = self.kwargs['course_id']
        Courses.objects.get(pk=course_id).delete()
        return redirect('admin-courses-index')

# Course Details
class CourseDetailsView(View):
    def get(self, request, course_id):
        course = Courses.objects.get(pk = course_id )
        contents = Content.objects.filter(course=course).order_by('order')
        materials = Materials.objects.filter(course=course)
        return render(request, 'zedny_admin/courses/details.html',{
            'course': course,
            'contents': contents,
            'materials': materials
        })


# # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Create Content
class CreateContentView(View):
    def dispatch(self, *args, **kwargs):
        method = self.request.POST.get('_method', '')
        if method == 'PUT':
            return self.put(*args, **kwargs)
        if method == 'DELETE':
            return self.destroy(*args, **kwargs)
        return super(CreateContentView, self).dispatch(*args, **kwargs)

    def get(self, request, course_id, content_id = None):
        if course_id is not None:
            cl = Content.objects.filter(course= course_id).order_by('order')
            clJSON = serializers.serialize('json', cl)
            if content_id is not None:
                instance = get_object_or_404(Content, id=content_id)
                # instance = Content.objects.get(pk=content_id)
                content = AddContentList(content=instance)
                if instance.type == 'Video':
                    context = {
                        'video': Videos.objects.get(pk=instance.type_id),
                        'content': content,
                        'step': 'content',
                        'contentList': clJSON,
                        'course_id': course_id,
                        'content_id': content_id
                    }
                elif content.type == 'Exam' or content.type == 'Quiz':
                    pass
                
            else:
                content = AddContentList
                context = {
                        'content': content,
                        'step': 'content',
                        'contentList': clJSON,
                        'course_id': course_id
                    }
            return render(request, 'zedny_admin/courses/create.html', context)
        return redirect('admin-courses-create')

    def post(self, request, course_id):
        import magic
        content = AddContentList(request.POST, request.FILES)
        print(request.FILES.get('cover'))
        if 'SaveNext' in request.POST or 'SaveNew' in request.POST:
            if content.is_valid():
                course = Courses.objects.get(pk=course_id)
                maxOrder = Content.objects.filter(course = course.id).order_by('-order').first()
                order = maxOrder.order if maxOrder else 0
                with transaction.atomic():
                    contentInstance = Content(
                        name = request.POST.get('name'),
                        descr = request.POST.get('descr'),
                        level = course.level,
                        duration = 0,
                        type = request.POST.get('type'),
                        type_id = 0,
                        order = (order + 1),
                        course = course
                    )
                    contentInstance.save()
                    videoType = magic.from_buffer(request.FILES.get('video').read(), mime=True)
                    if contentInstance.type == 'Video':
                        videoInstance = Videos(
                            name = request.POST.get('name'),
                            cover = request.FILES.get('cover'),
                            video = request.FILES.get('video'),
                            descr = request.POST.get('descr'),
                            level = course.level,
                            duration = 0,
                            video_type = videoType,
                            instructor = course.instructor,
                            course = course,
                            content = contentInstance,
                            age_access = course.age_access
                        )
                        videoInstance.save()
                        contentInstance.type_id = videoInstance.pk
                        contentInstance.save()
                    elif contentInstance.type == 'Quiz' or contentInstance.type == 'Exam':
                        pass
                if 'SaveNext' in request.POST:
                    print('SaveNext')
                    return HttpResponseRedirect(reverse('admin-materials-create', kwargs={'course_id':str(course_id)}))
                elif 'SaveNew' in request.POST:
                    print('SaveNew')
                    return HttpResponseRedirect(reverse('admin-contents-create', kwargs={'course_id':str(course_id)}))
        elif 'Skip' in request.POST:
            print('Skip')
            return HttpResponseRedirect(reverse('admin-materials-create', kwargs={'course_id':str(course_id)}))
        # if contents is not valid
        print('contents is not valid')
        cl = Content.objects.filter(course= course_id).order_by('order')
        clJSON = serializers.serialize('json', cl)
        return render(request, 'zedny_admin/courses/create.html',{
            'content': content,
            'step': 'content',
            'course_id': course_id,
            'contentList': clJSON
        })

    def put(self, *args, **kwargs):
        import magic
        course_id = self.kwargs['course_id']
        content_id = self.kwargs['content_id']
        instance = get_object_or_404(Content, id=content_id)
        content = AddContentList(self.request.POST, self.request.FILES, content = instance)
        if 'SaveNext' in self.request.POST or 'SaveNew' in self.request.POST:
            if content.is_valid():
                course = Courses.objects.get(pk=course_id)

                with transaction.atomic():
                    instance.name = self.request.POST.get('name')
                    instance.descr = self.request.POST.get('descr')
                    instance.level = self.request.POST.get('level')
                    instance.duration = self.request.POST.get('duration')
                    instance.type = self.request.POST.get('type')
                    instance.save()
                    
                    if instance.type == 'Video':
                        if self.request.FILES.get('video'):
                            videoType = magic.from_buffer(self.request.FILES.get('video').read(), mime=True)
                            videoInstance = Videos(name = instance.name, cover = self.request.FILES.get('cover'), video = self.request.FILES.get('video'), descr = instance.descr, level = course.level, duration = instance.duration, video_type = videoType, instructor = course.instructor, course = course, content = instance, age_access = course.age_access)
                            videoInstance.save()
                            instance.type_id = videoInstance.pk
                            instance.save()
                    elif contentInstance.type == 'Quiz' or contentInstance.type == 'Exam':
                        pass

                if 'SaveNext' in self.request.POST:
                    return HttpResponseRedirect(reverse('admin-materials-create', kwargs={'course_id':str(course_id)}))

                elif 'SaveNew' in self.request.POST:
                    return HttpResponseRedirect(reverse('admin-contents-create', kwargs={'course_id':str(course_id)}))

        elif 'Skip' in self.request.POST:
            return HttpResponseRedirect(reverse('admin-materials-create', kwargs={'course_id':str(course_id)}))
            
        # if contents is not valid
        print('contents is not valid')
        cl = Content.objects.filter(course= course_id).order_by('order')
        clJSON = serializers.serialize('json', cl)
        if instance.type == 'Video':
            context = {
                'video': Videos.objects.get(pk=instance.type_id),
                'content': content,
                'step': 'content',
                'contentList': clJSON,
                'course_id': course_id
            }
        elif content.type == 'Exam' or content.type == 'Quiz':
            pass
        return render(self.request, 'zedny_admin/courses/create.html', context)

class ContentDetailsView(View):
    def get(self, request, content_id):
        content = Content.objects.get(pk = content_id )
        materials = Materials.objects.filter(content=content_id)
        if content.type == 'Video':
            video = Videos.objects.get(pk=content.type_id)
            return render(request, 'zedny_admin/courses/content.html',{
                'content': content,
                'video': video,
                'materials': materials
            })
        elif content.type == 'Quiz' or content.type == 'Exam':
            pass

class ContentDeleteView(View):
    def post(self, request, content_id):
        content = Content.objects.get(pk=content_id)
        course = content.course
        content.delete()
        Contents = Content.objects.filter(course= course).order_by('order')
        i = 1
        for content in Contents:
            content.order = i
            i = i + 1
            content.save()
            print(i)
        return HttpResponseRedirect(reverse('admin-course-details', kwargs={'course_id':str(course.pk)}))


# # # # # # # # # # # # # # # # # # # # # # # # # # # 
class CreateMatrialView(View):
    def get(self, request, course_id):
        if course_id is not None:
            material = AddMaterials
            contents = Content.objects.filter(course= course_id).order_by('order')
            return render(request, 'zedny_admin/courses/create.html',{
                'material': material,
                'contents': contents,
                'step': 'material',
                'course_id': course_id
            })
        return redirect('admin-courses-create')

    def post(self, request, course_id):
        import magic
        material = AddMaterials(request.POST,request.FILES)
        if 'SaveFinish' in request.POST or 'SaveNew' in request.POST:
            if material.is_valid():
                mime = magic.from_buffer(request.FILES.get('file').read(), mime=True)
                if mime == 'application/pdf':
                    type = 'PDF'
                type = 'DOC'
                materialInstance = Materials(
                    name = request.POST.get('name'),
                    file = request.FILES.get('file'),
                    size = request.POST.get('size'),
                    type = type,
                    mime = mime
                )
                if request.POST.get('of') == 'Course':
                    materialInstance.course = Courses.objects.get(pk = course_id)
                elif request.POST.get('of') == 'Content':
                    materialInstance.content = Content.objects.get(pk = request.POST.get('content'))
                # Save Material 
                materialInstance.save()
                if 'SaveFinish' in request.POST:
                    return HttpResponseRedirect(reverse('admin-course-details', kwargs={'course_id':str(course_id)}))
                elif 'SaveNew' in request.POST:
                    return redirect('/contents/create?id=' + str(course_id))
        elif 'Finish' in request.POST:
            return HttpResponseRedirect(reverse('admin-course-details', kwargs={'course_id':str(course_id)}))
        # elif 'inlineFinish' in request.POST:

        
        # if materials is not valid
        contents = Content.objects.filter(course= request.GET.get('id')).order_by('order')
        return render(request, 'zedny_admin/courses/create.html',{
            'material': material,
            'course_id': course_id,
            'contents': contents,
            'step': 'material',
        })

class MatrialDeleteView(View):
    def post(self, request, material_id):
        material = Materials.objects.get(pk=material_id)
        if material.course is not None:
            course = material.course
            material.delete()
            return HttpResponseRedirect(reverse('admin-course-details', kwargs={'course_id':str(course.pk)}))
        elif material.content is not None:
            content = material.content
            material.delete()
            return HttpResponseRedirect(reverse('admin-contents-details', kwargs={'content_id':str(content.pk)}))

class MaterialDownloadView(View):
    def get(self, request, material_id):
        import os
        from django.conf import settings
        from wsgiref.util import FileWrapper
        from django.http import HttpResponse
        material = Materials.objects.get(pk=material_id)
        file_path = material.file.url
        print(material.file.url)
        file_wrapper = open(file_path,'r')
        # file_mimetype = mimetypes.guess_type(file_path)
        response = HttpResponse(file_wrapper, content_type=material.mime )
        # response['X-Sendfile'] = file_path
        # response['Content-Length'] = os.stat(file_path).st_size
        response['Content-Disposition'] = 'attachment; filename=%s/' % str(material.name) 
        return response
        