from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.urls.base import reverse_lazy, reverse
from django.views.generic.edit import CreateView
from db.models import Exam,Questions,Answers,InteractiveQuestion
from zedny import parameters
from zedny_admin.forms.exams import ExamForm,interactive_question_form
from zedny_admin.views.quest_view.question import question


@login_required(login_url='admin-login', redirect_field_name='next')
def tracking(request):
    # goTo = check_is_admin(request, show_error_page=False)
    # if goTo is not None:
    #     return redirect(goTo)

    return render(request, 'zedny_admin/courses/tracking.html')


def exams_index(request):
    # goTo = check_is_admin(request, show_error_page=False)
    # if goTo is not None:
    #     return redirect(goTo)
    items = Exam.objects.all();
    paginator = Paginator(items, 25)
    page = request.GET.get('page')
    items = paginator.get_page(page)
    return render(request, 'zedny_admin/exams/index.html', {'items': items})


def exam_create(request):
    # goTo = check_is_admin(request, show_error_page=False)
    # if goTo is not None:
    #     return redirect(goTo)

    if request.POST:
        data = request.POST
        form = ExamForm(request.POST)
        if form.is_valid():
            exam = form.save(commit=False)
            exam.save()
            return HttpResponseRedirect(reverse('admin-exams-index'))
    else:
        form = ExamForm()  # modelform_factory(AppUser, fields=('email', 'password'))

    return render(request, 'zedny_admin/exams/create_crispy.html', {'form': form})



def CreateInteractiveQuestion(request , exam_id):
    exam = Exam.objects.get(id=exam_id)
    if request.POST:
        inter_form = interactive_question_form(request.POST)
        if (inter_form.is_valid()):
            q_save = inter_form.save(commit=False)
            q_save.exam_id = exam.id
            q_save.save()
            return render(request, 'zedny_admin/exams/question/test_cases_templete.html',{'exam': exam , "quest" : q_save})
    else:
        inter_form = interactive_question_form()

    return render(request, 'zedny_admin/exams/question/create_interactive.html',{'exam': exam , 'inter_form' : inter_form })


def CreateTestCaseTemplete (request , exam_id):
    exam = Exam.objects.get(id=exam_id)
    print(request.POST.get('test_templete'))
    if request.POST:
        quest = request.POST.get('quest')
        print(quest)
        inter = InteractiveQuestion.objects.get(id=int(quest))
        inter.test_case_templete = request.POST.get('test_templete')
        inter.save()

    return render(request, 'zedny_admin/exams/question/create_interactive.html',{})


def exam_delete(request , exam_id) :
    exam = Exam.objects.get(id=exam_id)
    exam.delete()
    return redirect('/exams/index')


def CreateQuestion(request , exam_id):
    exam = Exam.objects.get(id=exam_id)

    if (exam.type == "Interactive") :
        questions = Questions.objects.filter(exam=exam.id)
        return render(request, 'zedny_admin/exams/question/interactive.html',{'exam': exam , 'questions' : questions })

    quest = question(request , exam)
    return render(request,'zedny_admin/exams/questions.html',quest.save_question())



def UpdateQuestions(request , exam_id) :
    exam = Exam.objects.get(id=exam_id)
    quest = question(request, exam)
    quest.update_question()
    return redirect('/exams/questions/edit/' + str(exam_id))

def DeleteQuestions(request , exam_id, quest_id) :
    b = Questions.objects.get(id=quest_id)
    b.delete()
    return redirect('/exams/questions/edit/' + str(exam_id))


