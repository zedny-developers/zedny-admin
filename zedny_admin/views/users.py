from datetime import datetime

from django.contrib import messages
from django.shortcuts import render, redirect

from db.models import UserRole, User, Role
from zedny_admin.forms.User import UserCreate


def user_create(request):
    if request.method == 'POST':
        user_form = UserCreate(request.POST)
        if user_form.is_valid():
            user_form.save()
            for role in user_form.cleaned_data['roles']:
                user_role = UserRole(user=user_form.instance, role=role)
                user_role.save()
            return redirect('admin-users-index')
        messages.error(request, 'There is error with inputs')
    user_form = UserCreate()
    return render(request, 'zedny_admin/users/create.html', context={'form': user_form, 'error': 'There is something with inputs'})


def user_index(request):
    users = User.objects.all().order_by('id')
    return render(request, 'zedny_admin/users/index.html', context={'users': users})


def user_update(request, id):
    current_user = User.objects.get(pk=id)
    if request.method == 'POST':
        form = UserCreate(request.POST or None, request.FILES or None, instance=current_user)
        if form.is_valid():
            current_user = form.save(commit=False)
            print(request.POST.get('date_of_birth'))
            current_user.fname = form.cleaned_data.get('fname')
            current_user.lname = form.cleaned_data.get('lname')
            current_user.email = form.cleaned_data.get('email')
            current_user.date_of_birth = form.cleaned_data.get('dat e_of_birth')
            current_user.roles.set(form.cleaned_data.get('roles'))
            current_user.save()
        return redirect('admin-users-index')
    user_form = UserCreate(instance=current_user)
    return render(request, 'zedny_admin/users/update.html', context={'form': user_form, 'user': current_user})


def user_delete(request, id):
    user = User.objects.get(pk=id)
    if user.delete():
        return redirect('admin-users-index')
    messages.error(request, 'There is a problem deleting')
    return redirect('admin-users-update', id=user.pk)
