from django.contrib import messages
from django.db import IntegrityError
from django.shortcuts import render, redirect

from db.models import Book
from zedny_admin.forms.book_summary import BookForm, BookSummaryForm


def book_create(request):
    if request.method == 'POST':
        book_form = BookForm(request.POST)
        if book_form.is_valid():
            book = book_form.save()
        else:
            return render(request, 'zedny_admin/books/create.html', context={'form': book_form})
        return redirect('admin-book-summary-create', id=book.id)
    book_form = BookForm()
    return render(request, 'zedny_admin/books/create.html', context={'form': book_form})

def book_index(request):
    books = Book.objects.all().order_by('id')
    return render(request, 'zedny_admin/books/index.html', context={'books': books})

def book_update(request, id):
    current_book = Book.objects.get(pk=id)
    if request.method == 'POST':
        form = BookForm(request.POST or None, request.FILES or None, instance=current_book)
        if form.is_valid():
            # current_book = form.save(commit=False)
            # current_user.fname = form.cleaned_data.get('fname')
            # current_user.lname = form.cleaned_data.get('lname')
            # current_user.email = form.cleaned_data.get('email')
            # current_user.date_of_birth = form.cleaned_data.get('dat e_of_birth')
            # current_user.roles.set(form.cleaned_data.get('roles'))
            current_book.save()
        return redirect('admin-books-index')
    book_form = BookForm(instance=current_book)
    return render(request, 'zedny_admin/books/update.html', context={'form': book_form, 'book': current_book})



def book_delete(request, id):
    book = Book.objects.get(pk=id)
    if book.delete():
        return redirect('admin-books-index')
    messages.error(request, 'There is a problem deleting')
    return redirect('admin-book-update', id=book.pk)


# Book Summaries
def book_summary_create(request, id):
    current_book = None
    if request.method == 'POST':
        print(request.GET)
        book_form = BookSummaryForm(request.POST)
        if book_form.is_valid():
            book_form.save()
        return render(request, 'zedny_admin/book_summary/index.html')
    book_form = BookSummaryForm()
    current_book = request.GET.get('book_id')
    return render(request, 'zedny_admin/book_summary/create.html', context={'form': book_form})