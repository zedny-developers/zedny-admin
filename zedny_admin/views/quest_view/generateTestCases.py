import json
import random

class generateTestCases():

    INT = "integer"
    BOOLEAN = "boolean"
    STRING = "string"
    CHAR = "character"
    FLOAT = "float"
    DIVIDE_RANGE = 3
    assign_val_element = {}

    def __init__(self , numberOfTestCases , JsonTemplete):
        self.numberOfTestCases = numberOfTestCases
        self.JsonTemplete =JsonTemplete


    def generate(self):
        element_dict = json.loads(self.JsonTemplete)
        for test_ind in range(self.numberOfTestCases):
            test = ""
            for key, value in element_dict.items():
                type = value["value"]
                max_length = int(value["max_length"])
                min_length = int(value["min_length"])

                if (value["type_no_linked"].lower() != "null"):
                    type_no_linked = value["type_no_linked"]
                    element_linked = value["element_linked"]
                    array_value = self.assign_val_element[element_linked]
                    for i in range(0 , array_value) :
                        val = self.generateBetween(min_length, max_length, type_no_linked, test_ind)
                        test += str(val)
                        test += " "
                    test+="\n"
                else:
                    val = self.generateBetween(min_length , max_length ,type ,test_ind)
                    test += str(val)
                    test+="\n"
                    self.assign_val_element[key] = val
            print(test)

    def generateBetween(self , min , max , type , test_ind ):
        section = int(test_ind/(self.numberOfTestCases / self.DIVIDE_RANGE))
        # print("(", end="")
        # print((section * step) + min, end="-")
        # print((section*step)+step+min, end=") ")
        if type.lower() == self.INT :
                step = int((max-min+1)/self.DIVIDE_RANGE)
                return random.randint((section*step)+min,(section*step)+step+min)
        elif (type.lower() == self.FLOAT):
                step = float((max-min+1)/self.DIVIDE_RANGE)
                return random.uniform((section*step)+min,(section*step)+step+min)


object = '{"E1":{"value":"Integer","min_length":"1","max_length":"10","type_no_linked":"null","element_linked":"null"},"E2":{"value":"Integer","min_length":"1","max_length":"10","type_no_linked":"null","element_linked":"null"},"E3":{"value":"Linked With Array","min_length":"1","max_length":"1000","type_no_linked":"Integer","element_linked":"E1"}}'
test = generateTestCases(3,object)
test.generate()
