from zedny_admin.forms.exams import ExamForm,QuestionForm,AnswerForm
from db.models import Exam,Questions,Answers
from .abstract_operations import abstract_operations


class MCQ(abstract_operations):

    def __init__(self , request , exam):
        self.request = request
        self.exam = exam

    def save_question(self, id = -1):
        ansList = self.changeEmptyAnsToZero(self.request.POST.getlist('score'))
        # validation
        quest_form = QuestionForm(self.request.POST)
        ans_form = AnswerForm(self.request.POST)

        if quest_form.is_valid() and ans_form.is_valid():
            #Save Question
            q_save = quest_form.save(commit=False)
            q_save.exam_id = self.exam.id
            if (id != -1):
                q_save.id = id
            q_save.save()
            #Save Answers
            for (i, j) in zip(self.request.POST.getlist('answer'),ansList ):
                answer = Answers(answer=i, question=q_save)
                if (j == 0):
                    answer.is_correct = False
                else:
                    answer.is_correct = True
                answer.score = int(j)
                answer.save()

        return quest_form ,ans_form


    def delete_question(self , quest_id):
        b = Questions.objects.get(id=quest_id)
        b.delete()

    def update_question(self):
        quest_id = int(self.request.POST.getlist('quest_id')[0])
        id = self.delete_question(quest_id)
        self.save_question(id)

    def LoadQuestions(self):
        questions = Questions.objects.filter(exam=self.exam.id)
        collect_ans = {}
        for i in questions:
            answers = Answers.objects.filter(question=i.id)
            collect_ans[i.id] = answers

        return collect_ans,questions


    def getRender(self):
        quest_form = QuestionForm()
        mcq_form = AnswerForm()
        collect_ans, questions = self.LoadQuestions()
        return {'quest_form': quest_form, 'ans_form': mcq_form, 'exam': self.exam, 'questions': questions,
                'collect_ans': collect_ans}

    def changeEmptyAnsToZero(self , ansList):
        for i in range(len(ansList)):
            if (ansList[i] == '') :
                ansList[i] = 0
            else:
                try:
                    ansList[i] =  int(ansList[i])
                except ValueError:
                     return ansList

        return ansList





