from abc import ABCMeta, abstractmethod



class abstract_operations(metaclass=ABCMeta):

    @abstractmethod
    def save_question(self, id = -1):
        pass

    @abstractmethod
    def delete_question(self , quest_id):
        pass

    @abstractmethod
    def update_question(self):
        pass

    @abstractmethod
    def getRender(self):
        pass

    @abstractmethod
    def LoadQuestions(self):
        pass





