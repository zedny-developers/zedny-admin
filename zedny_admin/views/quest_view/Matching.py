from zedny_admin.forms.exams import QuestionForm,AnswerMatchingForm
from db.models import Exam,Questions,Answers


class Matching():

    def __init__(self , request , exam):
        self.request = request
        self.exam = exam


    def save_question(self, id = -1):
        quest_form = QuestionForm(self.request.POST)
        ans_form = AnswerMatchingForm(self.request.POST)
        if quest_form.is_valid() and ans_form.is_valid():
            # Save Question
            q_save = quest_form.save(commit=False)
            q_save.exam_id = self.exam.id
            if (id != -1):
                q_save.id = id
            q_save.save()
            # Save Answers
            for (i, j) in zip(self.request.POST.getlist('answer'), self.request.POST.getlist('match_answer')):
                answer = Answers(answer=i, question=q_save)
                answer.is_correct = True
                answer.match_answer = j
                answer.save()

        return quest_form, ans_form



    def delete_question(self , quest_id):
        b = Questions.objects.get(id=quest_id)
        b.delete()


    def update_question(self):
        quest_id = int(self.request.POST.getlist('quest_id')[0])
        id = self.delete_question(quest_id)
        self.save_question(id)


    def getRender(self):
        quest_form = QuestionForm()
        match_form = AnswerMatchingForm()
        collect_ans, questions = self.LoadQuestions()
        return {'quest_form': quest_form, 'ans_form': match_form, 'exam': self.exam, 'questions': questions,
                'collect_ans': collect_ans}

    def LoadQuestions(self):
        questions = Questions.objects.filter(exam=self.exam.id)
        collect_ans = {}
        for i in questions:
            answers = Answers.objects.filter(question=i.id)
            collect_ans[i.id] = answers

        return collect_ans, questions






