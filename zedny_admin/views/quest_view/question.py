from zedny_admin.forms.exams import QuestionForm,AnswerForm,AnswerMatchingForm
from db.models import Exam,Questions,Answers
from .MCQ import MCQ
from .Matching import Matching

class question ():
    MCQ = "MCQ"
    MATCHING = "Matching"
    COMPLETE = "Complete"

    def __init__(self,request,exam):
        self.request = request
        self.exam = exam
        self.pre_objects()


    def pre_objects(self):
        if self.exam.type == self.MCQ or self.exam.type == self.COMPLETE:
            self.mcq = MCQ(self.request, self.exam)
        elif self.exam.type == self.MATCHING:
            self.matching = Matching(self.request, self.exam)


    def save_question(self):
        if self.request.POST :
            if self.exam.type == self.MCQ or self.exam.type == self.COMPLETE:
                quest_form, mcq_form =self.mcq.save_question()
                collect_ans, questions =self.mcq.LoadQuestions()
                return {'quest_form': quest_form, 'ans_form': mcq_form, 'exam': self.exam,'questions': questions,'collect_ans': collect_ans}
            elif self.exam.type == self.MATCHING:
                quest_form, form = self.matching.save_question()
                collect_ans, questions = self.matching.LoadQuestions()
                return {'quest_form': quest_form, 'ans_form': form, 'exam': self.exam, 'questions': questions,
                        'collect_ans': collect_ans}


        else:
            if self.exam.type == self.MCQ or self.exam.type == self.COMPLETE:
                return self.mcq.getRender()
            elif self.exam.type == self.MATCHING:
                print(self.matching.getRender())
                return self.matching.getRender()


    def delete_question(self , quest_id):
        b = Questions.objects.get(id=quest_id)
        b.delete()

    def update_question(self):
        if self.exam.type == self.MCQ or self.exam.type == self.COMPLETE:
            quest_form = QuestionForm(self.request.POST)
            ans_form = AnswerForm(self.request.POST)
            if quest_form.is_valid() and ans_form.is_valid():
                self.mcq.update_question()
        if self.exam.type == self.MATCHING:
            quest_form = QuestionForm(self.request.POST)
            ans_form = AnswerMatchingForm(self.request.POST)
            if quest_form.is_valid() and ans_form.is_valid():
                self.matching.update_question()









