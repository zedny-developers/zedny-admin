from django.contrib import messages
from django.shortcuts import render, redirect

from db.models import Role, UserRole, Permissions
from zedny import parameters
from zedny_admin.forms.roles import RoleForm, RoleUserForm, PermissionForm


# @login_required(login_url='admin-login', redirect_field_name='next')
def roles_create(request):
    if request.method == 'POST':
        roles_form = RoleForm(request.POST)
        if roles_form.is_valid():
            role = roles_form.save()
            if request.POST.get('permissions[]'):
                print(request.POST.getlist('permissions[]'))
                for perm in request.POST.getlist('permissions[]'):
                    permission = Permissions(target=perm.split('|')[0], action=perm.split('|')[1], role=role)
                    permission.save()
            return redirect('admin-roles-index')
        else:
            return render(request, 'zedny_admin/roles/create.html',
                          context={'form': roles_form, 'error': 'There is something with inputs'})
    else:
        actions = dict(parameters.PERMISSIONS_ACTIONS_LIST)
        targets = dict(parameters.PERMISSIONS_TARGET_LIST)
        print(actions)
        form = RoleForm()
        return render(request, 'zedny_admin/roles/create.html',
                      context={'form': form, 'actions': actions, 'targets': targets})


def roles_update(request, id):
    current_role = Role.objects.get(pk=id)
    if request.method == 'POST':
        Role.objects.filter(pk=id).update(name=request.POST['name'], descr=request.POST['descr'])
        if request.POST.get('permissions[]'):
            Permissions.objects.filter(role=current_role).delete()
            for perm in request.POST.getlist('permissions[]'):
                permission = Permissions(target=perm.split('|')[0], action=perm.split('|')[1], role=current_role)
                permission.save()
        return redirect('admin-roles-index')
    else:
        actions = dict(parameters.PERMISSIONS_ACTIONS_LIST)
        targets = dict(parameters.PERMISSIONS_TARGET_LIST)
        form = RoleForm(instance=current_role)
        permissions = list(Permissions.objects.values('target', 'action').filter(role=current_role))
        for x in range(len(permissions)):
            permissions[x] = '{}|{}'.format(permissions[x]['target'], permissions[x]['action'])
        return render(request, 'zedny_admin/roles/roles_edit.html',
                      context={'form': form, 'actions': actions, 'targets': targets, 'permissions': permissions,
                               'role': current_role})


def roles_index(request):
    qs = Role.objects.all().order_by('id')
    return render(request, 'zedny_admin/roles/index.html', context={'roles': qs})


def user_roles_create(request):
    if request.method == 'POST':
        roles_form = RoleUserForm(request.POST)
        if roles_form.is_valid:
            roles_form.save()
            return redirect('admin-user-roles-index')
        else:
            return render(request, 'zedny_admin/roles/create_user_role.html',
                          context={'form': roles_form, 'error': 'There is something with inputs'})
    else:
        roles_form = RoleUserForm()
    return render(request, 'zedny_admin/roles/create_user_role.html', context={'form': roles_form})


def userroles_index(request):
    qs = UserRole.objects.all().order_by('id')
    return render(request, 'zedny_admin/roles/index_user_role.html', context={'user_roles': qs})


def permission_create(request):
    if request.method == 'POST':
        permission_form = PermissionForm(request.POST)
        if permission_form.is_valid:
            permission_form.save()
            return redirect('admin-permissions-index')
        else:
            return render(request, 'zedny_admin/roles/permission_create.html',
                          context={'form': permission_form, 'error': 'There is something with inputs'})
    else:
        permission_form = PermissionForm()
    return render(request, 'zedny_admin/roles/permission_create.html', context={'form': permission_form})


def permissions_index(request):
    qs = Permissions.objects.all().order_by('id')
    return render(request, 'zedny_admin/roles/permission_index.html', context={'permissions': qs})


def role_delete(request, id):
    role = Role.objects.get(pk=id)
    if role.delete():
        return redirect('admin-roles-index')
    messages.error(request, 'There is a problem deleting')
    return redirect('admin-role-update', id=role.pk)
