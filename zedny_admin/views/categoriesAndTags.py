from django.contrib.auth.decorators import login_required
from django.views import View
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from db.models import Category, Tag
from zedny_admin.forms.courses import *
from django.urls import reverse
from django.db import transaction
from django.core import serializers
from django.db.models import Q
from zedny_admin.serializers.categoriesAndTags import *
from django.utils.six import BytesIO
from rest_framework.parsers import JSONParser
from django.http import QueryDict

class CategoryResourceView(View):
    def dispatch(self, *args, **kwargs):
        method = self.request.POST.get('_method', '')
        if self.request.method == 'GET':
            if 'create' in self.request.path:
                return self.form(*args, **kwargs)
            if 'ajax' in self.request.path:
                return self.ajax(*args, **kwargs)
            return self.index(*args, **kwargs)
        if method == 'POST' or self.request.method == 'POST':
            return self.store(*args, **kwargs)
        if method == 'PUT' or self.request.method == 'PUT':
            return self.update(*args, **kwargs)
        if method == 'DELETE' or self.request.method == 'DELETE':
            return self.destroy(*args, **kwargs)
        return super(CategoryResourceView, self).dispatch(*args, **kwargs)
  
    def index(self, *args, **kwargs):
        return render(self.request, 'zedny_admin/categoriesAndTags/index.html', {
           'categories': Category.objects.all(),
           'tags': Tag.objects.all()
        })

    def ajax(self, *args, **kwargs):
        if self.request.GET.get('filter') == 'parents':
            categories = Category.objects.filter(Q(parent=0)|Q(parent=None))
            categoriesobject = CategoriesSerializer(categories, many=True)
            return JsonResponse(categoriesobject.data, safe=False)

        elif self.request.GET.get('filter') == 'children':
            categories = Category.objects.filter(parent=self.request.GET.get('id'))
            categoriesobject = CategoriesSerializer(categories, many=True)
            return JsonResponse(categoriesobject.data, safe=False)

        elif self.request.GET.get('filter') == 'all':
            categories = Category.objects.all()
            categoriesobject = CategoriesSerializer(categories, many=True)
            return JsonResponse(categoriesobject.data, safe=False)

    def form(self, *args, **kwargs):
        return HttpResponse('this is a form')

    def store(self, *args, **kwargs):
        if self.request.POST.get('filter') == 'ajaxCreate':
            Category(name=self.request.POST.get('name'),parent=self.request.POST.get('parent')).save()
            categories = Category.objects.filter(Q(parent=0)|Q(parent=None))
            categoriesobject = CategoriesSerializer(categories, many=True)
            return JsonResponse(categoriesobject.data, safe=False)

    def update(self, *args, **kwargs):
        if QueryDict(self.request.body).get('filter') == 'ajaxUpdate':
            id = int(QueryDict(self.request.body).get('id'))
            category = Category.objects.get(pk=id)
            category.name = QueryDict(self.request.body).get('name')
            category.parent = QueryDict(self.request.body).get('parent')
            category.save()
            categories = Category.objects.filter(Q(parent=0)|Q(parent=None))
            categoriesobject = CategoriesSerializer(categories, many=True)
            return JsonResponse(categoriesobject.data, safe=False)
            
    def destroy(self, *args, **kwargs):
        id = int(QueryDict(self.request.body).get('id'))
        category = Category.objects.get(pk=id)
        subCategories = Category.objects.filter(parent=id)
        for cat in subCategories:
            cat.parent = category.parent
            cat.save()
        category.delete()
        categories = Category.objects.filter(Q(parent=0)|Q(parent=None))
        categoriesobject = CategoriesSerializer(categories, many=True)
        return JsonResponse(categoriesobject.data, safe=False)

class TagResourceView(View):
    def dispatch(self, *args, **kwargs):
        method = self.request.POST.get('_method', '')
        if self.request.method == 'GET':
            if 'create' in self.request.path:
                return self.form(*args, **kwargs)
            if 'ajax' in self.request.path:
                return self.ajax(*args, **kwargs)
            return self.index(*args, **kwargs)
        if method == 'POST' or self.request.method == 'POST':
            return self.store(*args, **kwargs)
        return super(CategoryResourceView, self).dispatch(*args, **kwargs)
  
    def index(self, *args, **kwargs):
        return render(self.request, 'zedny_admin/categoriesAndTags/index.html', {
           'categories': Category.objects.all(),
           'tags': Tag.objects.all()
        })

    def ajax(self, *args, **kwargs):
        if self.request.GET.get('filter') == 'all':
            tags = Tag.objects.all()
            tagsobject = TagsSerializer(tags, many=True)
            return JsonResponse(tagsobject.data, safe=False)
        elif self.request.GET.get('filter') == 'search':
            tags = Tag.objects.filter(name__icontains=self.request.GET.get('searchKey'))
            tagsobject = TagsSerializer(tags, many=True)
            return JsonResponse(tagsobject.data, safe=False)

    def store(self, *args, **kwargs):
        if self.request.POST.get('filter') == 'ajaxCreate':
            try:
                obj = Tag.objects.get(name=self.request.POST.get('name'))
            except Tag.DoesNotExist:
                obj = Tag(name=self.request.POST.get('name')).save()
                
            tags = Tag.objects.all()
            tagsobject = TagsSerializer(tags, many=True)
            return JsonResponse(tagsobject.data, safe=False)