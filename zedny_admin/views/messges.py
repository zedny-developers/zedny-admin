from django.contrib.auth.decorators import login_required
from django.shortcuts import render


@login_required(login_url='admin-login', redirect_field_name='next')
def messages(request):
    # goTo = check_is_admin(request, show_error_page=False)
    # if goTo is not None:
    #     return redirect(goTo)

    return render(request, 'zedny_admin/messages/messages.html')