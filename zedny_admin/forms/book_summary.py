from django import forms
from django.views.generic.base import View, TemplateResponseMixin
from django.views.generic.edit import FormMixin, ProcessFormView

from db.models import BookSummery, Book


class BookForm(forms.ModelForm):
    class Meta:
        model = Book

        fields = ['name', 'descr', 'cover', 'author', 'year']


class BookSummaryForm(forms.ModelForm):
    class Meta:
        model = BookSummery
        fields = ['book', 'video', 'status', 'level']
