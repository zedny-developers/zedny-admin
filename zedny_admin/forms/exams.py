from django import forms

from db.models import Exam, Questions, Answers, InteractiveQuestion


class ExamForm(forms.ModelForm):
    # release_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)

    class Meta:
        model = Exam
        fields = ["name", "descr", "level", "type"]

    def __init__(self, *args, **kwargs):
        super(ExamForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Questions
        fields = ["question"]


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answers
        fields = ["answer", "score"]


class AnswerMatchingForm(forms.ModelForm):
    class Meta:
        model = Answers
        fields = ["match_answer", "answer"]


class interactive_question_form(forms.ModelForm):
    class Meta:
        model = InteractiveQuestion
        fields = ["name", "quest_text", "input_text", "output_text", "max_score", "max_memory_limit", "max_time_limit"]
