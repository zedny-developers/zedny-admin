from django import forms
from django.forms import inlineformset_factory

from db.models import Role, UserRole, Permissions
from zedny import settings


class RoleForm(forms.ModelForm):
    class Meta:
        model = Role
        fields = ["name", "descr",]


class RoleUserForm(forms.ModelForm):
    class Meta:
        model = UserRole
        fields = ["user", "role",]


class PermissionForm(forms.ModelForm):
    class Meta:
        model = Permissions
        fields = ["role", "target", 'action']