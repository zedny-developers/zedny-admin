from db.models import User

__author__ = 'mahmoud'

from django import forms


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'password']


class RegisterForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['fname', 'lname', 'email', 'password']


class UserCreate(forms.ModelForm):
    class Meta:
        model = User
        fields = ['fname', 'lname', 'email', 'password', 'date_of_birth', 'facebook_id', 'parent', 'age_access',
                  'institution', 'accountType', 'gender', 'img', 'roles']
