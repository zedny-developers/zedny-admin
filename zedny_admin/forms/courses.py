from django import forms
from db.models import Courses, User, Content, Materials, TranScripts, Subtitles, Videos
from zedny import parameters
from crispy_forms.helper import FormHelper
from django.core.validators import EMPTY_VALUES

class CoursesForm(forms.Form):
    name = forms.CharField(required=True)
    descr = forms.CharField(required=True)
    slug = forms.SlugField(allow_unicode=True, required=False)
    instructor = forms.ModelChoiceField(queryset=User.objects.all(), required=True)
    cover = forms.ImageField(required=False)
    status = forms.ChoiceField(choices=parameters.STATUS_LIST,required=True)
    level = forms.ChoiceField(choices=parameters.SUBSCRIPTION_LEVEL_LIST,required=True)
    age_access = forms.ChoiceField(choices=parameters.AGE_ACCESS_LIST,required=True)
    has_offer = forms.BooleanField(required=False)
    offer_discount_perc = forms.IntegerField()
    offer_start_date = forms.DateField()
    offer_end_date = forms.DateField()
    

    def clean(self):
        has_offer = self.cleaned_data.get('has_offer')
        if has_offer:
            if self.cleaned_data.get('offer_discount_perc') is None:
               self.add_error('offer_discount_perc', forms.ValidationError("this field is required if course has offer"))
            if self.cleaned_data.get('offer_start_date') is None:
                self.add_error('offer_start_date', forms.ValidationError("this field is required if course has offer"))
            if self.cleaned_data.get('offer_end_date') is None:
                self.add_error('offer_end_date', forms.ValidationError("this field is required if course has offer"))
        return self.cleaned_data

    
class AddContentList(forms.Form):
    name = forms.CharField(required=True)
    descr = forms.CharField(required=True)
    duration = forms.IntegerField(required=False)
    type = forms.ChoiceField(choices=parameters.CONTENT_TYPE_LIST,required=True)
    exam = forms.IntegerField(required=False)
    video = forms.FileField(required=False)
    
   
    def __init__(self,*args,**kwargs):
        instance = kwargs.pop('content', None)
        super(AddContentList, self).__init__(*args,**kwargs)
        if instance:
            self.fields['name'].initial = instance.name
            self.fields['descr'].initial = instance.descr
            self.fields['duration'].initial = instance.duration
            self.fields['type'].initial = instance.type
            if instance.type == 'Video':
                video = Videos.objects.get(pk=instance.type_id)
                self.fields['video'].initial = video.video
                self.fields['cover'].initial = video.cover
            elif instance.type == 'Exam' or instance.type == 'Quiz':
                pass
        

    def clean(self):
        type = self.cleaned_data.get('type')
        if self.cleaned_data.get('type') is not None:
            if type == 'Video':
                if self.cleaned_data.get('video') is None:
                    self.add_error('video', forms.ValidationError("This field is required if type is Video."))
                if self.cleaned_data.get('cover') is None:
                    self.add_error('cover', forms.ValidationError("This field is required if type is Video."))
            elif type == 'Exam' or type == 'Quiz':
                if self.cleaned_data.get('video') is None:
                    self.add_error('video', forms.ValidationError("This field is required if type is 'Exam' or 'Quiz'!"))
        return self.cleaned_data

class AddMaterials(forms.Form):
    name = forms.CharField(required=True)
    file = forms.FileField(required=True)
    size = forms.IntegerField(required=True)
    course = forms.Field(required=False)
    of = forms.ChoiceField(choices=(
            ('Course','Course'),
            ('Content','Content')
        ),required=True)
    content = forms.Field(required=False)

    def clean(self):
        if self.cleaned_data.get('of') == 'Content':
            if self.cleaned_data.get('content') is None:
                self.add_error('content', forms.ValidationError("This field is required if 'of' field  is 'Content'!"))




# class AddTranScripts(forms.ModelForm):
#     class Meta:
#         model = TranScripts
#         fields = (
#             'name', 'type', 'video', 'course'
#         )

# class AddSubtitles(forms.ModelForm):
#     class Meta:
#         model = Subtitles
#         fields = (
#             'name', 'file', 'ext', 'mime', 'size', 'lang', 'video'
#         )

