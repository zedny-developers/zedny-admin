from django.apps import AppConfig


class AdminConfig(AppConfig):
    name = 'zedny_admin'
