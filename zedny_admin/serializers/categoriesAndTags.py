from rest_framework import serializers
from db.models import Category, Tag

class CategoriesSerializer(serializers.ModelSerializer):
    hasChildren = serializers.SerializerMethodField()

    def get_hasChildren(self, obj):
        children = Category.objects.filter(parent = obj.pk).count()
        if children == 0:
            return False
        return True

    class Meta:
        model = Category
        fields = ('id', 'name', 'parent', 'hasChildren')

    def create(self, validated_data):
        return Category(**validated_data)

class TagsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name')

    def create(self, validated_data):
        return Tag(**validated_data)
