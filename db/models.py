# Create your models here.
import os
import uuid
from datetime import date

from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.db.models.base import Model
from django.db.models.fields import BigIntegerField
# Create your models here.
from django.dispatch.dispatcher import receiver
from django.utils.text import slugify
from django.utils.timezone import now
from sorl.thumbnail.fields import ImageField

from zedny import parameters


# Create your models here.
class AppUserManager(BaseUserManager):
    def create_user(self, email, password=None):  # date_of_birth, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            # date_of_birth=date_of_birth,
            # date_of_birth=NULL,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):  # date_of_birth, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(email,
                                password=password,
                                # date_of_birth=date_of_birth
                                # date_of_birth=NULL
                                )
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'profiles' + datePart + str(uuid.uuid4()) + extension
        return result

    class Meta:
        db_table = "users"

    id = models.BigAutoField(primary_key=True)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    facebook_id = BigIntegerField(null=True, blank=True)
    # parent control
    parent = models.ForeignKey('User', on_delete=models.CASCADE, null=True, blank=True, default=None)
    age_access = models.CharField(max_length=20, choices=parameters.AGE_ACCESS_LIST,
                                  default=parameters.AGE_ACCESS_BLOCK)
    institution = models.ForeignKey('Institutions', on_delete=models.CASCADE, default=None, null=True, blank=True)
    # username = models.CharField(max_length=50, default=uuid.uuid4, editable=False)
    date_of_birth = models.DateField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    accountType = models.CharField(max_length=20, choices=parameters.membersGroupsList,
                                   default=parameters.MEMBER_MEMBER)
    created_at = models.DateField(default=now, null=True, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)
    ##
    fname = models.CharField(max_length=100, null=True, blank=False)
    lname = models.CharField(max_length=100, null=True, blank=False)
    is_mail_active = models.BooleanField(default=False)
    account_status = models.CharField(max_length=20, null=True, choices=parameters.ACCOUNT_STATUS_LIST,
                                      default=parameters.ACCOUNT_STATUS_NOT_VERIFIED)
    activiation_code = models.CharField(max_length=50, default=uuid.uuid4, editable=False)
    gender = models.CharField(max_length=100, default='', blank=True, null=True)
    # img = ImageField(upload_to='profiles/%Y/%d/%m/', null=True, default='profiles/male.jpg')
    img = ImageField(upload_to=get_upload_to, null=True, default='profiles/default_user.jpg')

    roles = models.ManyToManyField(to='Role', related_name="user_roles")

    objects = AppUserManager()
    USERNAME_FIELD = 'email'

    # REQUIRED_FIELDS = ['date_of_birth']

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):  # __unicode__ on Python 2
        return self.fname + ' ' + self.lname

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


# These two auto-delete files from filesystem when they are unneeded:
@receiver(models.signals.post_delete, sender=User)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.img:
        if os.path.isfile(instance.img.path):
            os.remove(instance.img.path)


@receiver(models.signals.pre_save, sender=User)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file = User.objects.get(pk=instance.pk).img
    except User.DoesNotExist:
        return False

    new_file = instance.img
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class Institutions(models.Model):
    class Meta:
        db_table = "institutions"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=100, unique=True, null=False, blank=False)
    descr = models.TextField(null=True, blank=True)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.name


# Institution groups
class InstitutionGroup(models.Model):
    class Meta:
        db_table = "institution_group"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=100, unique=True, null=False, blank=False)
    institution = models.ForeignKey(Institutions, blank=False, null=False, on_delete=models.CASCADE)
    users = models.ManyToManyField(User, related_name='institutions_groups_users')
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.name


class Role(models.Model):
    class Meta:
        db_table = "roles"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=100, unique=True, null=False, blank=False)
    descr = models.TextField(null=True, blank=True)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.name


class UserRole(models.Model):
    class Meta:
        db_table = "user_roles"

    id = models.BigAutoField(primary_key=True)
    role = models.ForeignKey(Role, related_name='user_roles_role', on_delete=models.CASCADE, null=False)
    user = models.ForeignKey(User, related_name='user_roles_user', on_delete=models.CASCADE)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)


class Permissions(models.Model):
    class Meta:
        db_table = "permissions"
        unique_together = ('target', 'action', 'role')

    id = models.BigAutoField(primary_key=True)
    target = models.CharField(max_length=100, default=parameters.PERMISSIONS_TARGET_USER,
                              choices=parameters.PERMISSIONS_TARGET_LIST,
                              null=False, blank=False)
    action = models.CharField(max_length=100, default=parameters.PERMISSIONS_TARGET_USER,
                              choices=parameters.PERMISSIONS_ACTIONS_LIST,
                              null=False, blank=False)
    role = models.ForeignKey(Role, on_delete=models.CASCADE,
                             related_name='roles_permissions', null=False)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.target + " " + self.action


class Country(Model):
    class Meta:
        db_table = "countries"

    id = models.CharField(max_length=2, primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.name


class Category(Model):
    class Meta:
        db_table = "categories"

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    parent = models.IntegerField(default=None, null=True, blank=True)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.name


class ItemCat(Model):
    class Meta:
        db_table = "items_cats"

    id = models.BigAutoField(primary_key=True)
    type = models.CharField(max_length=255, blank=False, null=False,
                            choices=parameters.TYPE_LIST,
                            default=parameters.TYPE_COURSE)
    type_id = models.BigIntegerField(default=None, null=False, blank=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='item_cat')
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class Tag(Model):
    class Meta:
        db_table = "tags"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class ItemTag(Model):
    class Meta:
        db_table = "items_tags"

    id = models.BigAutoField(primary_key=True)
    type = models.CharField(max_length=255, blank=False, null=False,
                            choices=parameters.TYPE_LIST,
                            default=parameters.TYPE_COURSE)
    type_id = models.BigIntegerField(default=None, null=False, blank=False)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE, related_name='item_tag')
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class Book(Model):
    class Meta:
        db_table = "books"
        unique_together = ('name', 'author')

    def get_upload_to_cover(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'books_covers' + datePart + str(uuid.uuid4()) + extension
        return result

    name = models.CharField(max_length=255, blank=False, null=False)
    descr = models.TextField(blank=False, null=False)
    cover = ImageField(upload_to=get_upload_to_cover, null=True, default='books_covers/default.jpg')
    author = models.ForeignKey('Author', on_delete=models.CASCADE)
    year = models.CharField(max_length=255, blank=False, null=False, default='')

    def __str__(self):
        return self.name


class Author(Model):
    class Meta:
        db_table = "author"

    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'authors' + datePart + str(uuid.uuid4()) + extension
        return result

    name = models.CharField(max_length=255, blank=False, null=False)
    image = ImageField(upload_to=get_upload_to, null=True, default='authors/default.jpg')
    date_of_birth = models.DateField()
    email = models.EmailField(blank=True, null=True)
    phone_number = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name


class BookSummery(Model):
    class Meta:
        db_table = "book_summary"

    id = models.BigAutoField(primary_key=True)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    video = models.ForeignKey('Videos', on_delete=models.CASCADE)
    status = models.TextField(max_length=50, default=parameters.STATUS_INIT,
                              choices=parameters.STATUS_LIST,
                              null=False, blank=False)
    level = models.TextField(max_length=50, default=parameters.SUBSCRIPTION_LEVEL_FREE,
                             choices=parameters.SUBSCRIPTION_LEVEL_LIST,
                             null=False, blank=False)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.book.name


# These two auto-delete files from filesystem when they are unneeded:
@receiver(models.signals.post_delete, sender=Book)
def auto_delete_book_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.cover:
        if os.path.isfile(instance.cover.path):
            os.remove(instance.cover.path)


@receiver(models.signals.pre_save, sender=Book)
def auto_delete_book_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file_object = Book.objects.get(pk=instance.pk)
        old_file_cover = old_file_object.cover
    except Book.DoesNotExist:
        return False

    new_file_cover = instance.cover
    if not old_file_cover == new_file_cover:
        if os.path.isfile(old_file_cover.path):
            os.remove(old_file_cover.path)


class QuestionType(Model):
    class Meta:
        db_table = "question_types"

    id = models.BigAutoField(primary_key=True)
    type = models.CharField(max_length=150, blank=True, null=True)
    is_active = models.BooleanField(null=False, default=True, blank=True)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class Exam(Model):
    class Meta:
        db_table = "exams"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    descr = models.TextField(blank=False, null=False)
    level = models.TextField(max_length=50, default=parameters.SUBSCRIPTION_LEVEL_FREE,
                             choices=parameters.SUBSCRIPTION_LEVEL_LIST,
                             null=False, blank=False)
    type = models.TextField(max_length=50, default=parameters.EXAM_TYPE_MCQ,
                            choices=parameters.EXAM_TYPE_LIST,
                            null=False, blank=False)
    course = models.ForeignKey('Courses', on_delete=models.SET_NULL, default=None, null=True, blank=True)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class InteractiveQuestion(Model):
    class Meta:
        db_table = "interactive_questions"

    id = models.BigAutoField(primary_key=True)
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE, null=False, blank=False)
    name = models.CharField(max_length=250, blank=False, null=False)
    quest_text = models.TextField(blank=False, null=False)
    input_text = models.TextField(blank=False, null=False)
    output_text = models.TextField(blank=False, null=False)
    optimal_accepted_code = models.TextField(null=False)
    test_case_templete = models.TextField(null=False)
    max_memory_limit = models.IntegerField(default=0)
    max_time_limit = models.IntegerField(default=0)
    optimal_time_limit = models.IntegerField(default=0, blank=True)
    optimal_memory_limit = models.IntegerField(default=0, blank=True)
    max_score = models.IntegerField(default=0)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class TestCase(Model):
    class Meta:
        db_table = "test_cases"

    id = models.BigAutoField(primary_key=True)
    question = models.ForeignKey(InteractiveQuestion, on_delete=models.CASCADE, null=False, blank=False)
    input = models.TextField(blank=False, null=False)
    output = models.TextField(blank=False, null=False)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class Questions(Model):
    class Meta:
        db_table = "questions"

    id = models.BigAutoField(primary_key=True)
    question = models.CharField(max_length=255, blank=False, null=False)
    quest_type = models.TextField(max_length=50, default=parameters.EXAM_TYPE_MCQ,
                                  choices=parameters.EXAM_TYPE_LIST,
                                  null=False, blank=False)
    # question_type = models.ForeignKey(QuestionType, on_delete=models.CASCADE, null=False, blank=False)
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE, null=False, blank=False)
    answer_time = models.PositiveSmallIntegerField(default=2)
    difficulty = models.PositiveSmallIntegerField(default=5)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class Answers(Model):
    class Meta:
        db_table = "answers"

    id = models.BigAutoField(primary_key=True)
    question = models.ForeignKey(Questions, on_delete=models.CASCADE, null=False, blank=False)
    answer = models.CharField(max_length=50, null=False, blank=False)
    match_answer = models.CharField(max_length=50, default="", null=False, blank=False)
    is_correct = models.BooleanField(default=False)
    score = models.IntegerField(default=0, blank=True)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class Courses(Model):
    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.name, allow_unicode=True)
        super(Courses, self).save(*args, **kwargs)

    def get_upload_to_cover(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'courses_covers' + datePart + str(uuid.uuid4()) + extension
        return result

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('admin-course-details', args=[str(self.id)])

    class Meta:
        db_table = "courses"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    slug = models.SlugField(max_length=255, default=None, allow_unicode=True)
    descr = models.TextField(blank=False, null=False)
    instructor = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL, default=None)
    cover = ImageField(upload_to=get_upload_to_cover, null=True, default='courses_covers/default.jpg')
    status = models.TextField(max_length=50, default=parameters.STATUS_INIT,
                              choices=parameters.STATUS_LIST,
                              null=False, blank=False)
    level = models.TextField(max_length=50, default=parameters.SUBSCRIPTION_LEVEL_FREE,
                             choices=parameters.SUBSCRIPTION_LEVEL_LIST,
                             null=False, blank=False)
    duration = models.IntegerField(default=0, blank=True, null=False)
    # Offer
    has_offer = models.BooleanField(default=False)
    offer_discount_perc = models.PositiveIntegerField(default=0, blank=True)
    offer_start_date = models.DateField(default=None, null=True, blank=True)
    offer_end_date = models.DateField(default=None, null=True, blank=True)

    age_access = models.CharField(max_length=20, choices=parameters.AGE_ACCESS_LIST,
                                  default=parameters.AGE_ACCESS_ALLOW)

    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


# These two auto-delete files from filesystem when they are unneeded:
@receiver(models.signals.post_delete, sender=Courses)
def auto_delete_course_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.cover:
        if os.path.isfile(instance.cover.path):
            os.remove(instance.cover.path)


@receiver(models.signals.pre_save, sender=Courses)
def auto_delete_course_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file_object = Courses.objects.get(pk=instance.pk)
        old_file_cover = old_file_object.cover
    except Courses.DoesNotExist:
        return False

    new_file_cover = instance.cover
    if not old_file_cover == new_file_cover:
        if os.path.isfile(old_file_cover.path):
            os.remove(old_file_cover.path)


@receiver(models.signals.post_save, sender=Courses)
def auto_calculate_course_duration(sender, instance, **kwargs):
    """update course duration based on the sum of contents durations"""
    if not instance.pk:
        return False
    course = Courses.objects.filter(pk=instance.pk).update(
        duration=Content.objects.filter(course=instance.pk).aggregate(models.Sum('duration'))
    )


class Content(Model):
    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.title)
        super(Content, self).save(*args, **kwargs)

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('admin-contents-details', args=[str(self.id)])

    class Meta:
        db_table = "contents"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    slug = models.SlugField(max_length=255, default=None, allow_unicode=True)
    descr = models.TextField(blank=False, null=False)
    level = models.TextField(max_length=50, default=parameters.SUBSCRIPTION_LEVEL_FREE,
                             choices=parameters.SUBSCRIPTION_LEVEL_LIST,
                             null=False, blank=False)
    duration = models.IntegerField(default=0, blank=True, null=False)
    type = models.TextField(max_length=50, default=parameters.CONTENT_TYPE_VIDEO,
                            choices=parameters.CONTENT_TYPE_LIST,
                            null=False, blank=False)
    type_id = models.BigIntegerField(null=False, blank=False)
    course = models.ForeignKey(Courses, on_delete=models.CASCADE, default=None, null=True, blank=True)
    order = models.IntegerField(default=1, null=True, blank=True)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class Videos(Model):
    def get_upload_to_cover(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'videos_covers' + datePart + str(uuid.uuid4()) + extension
        return result

    def get_upload_to_video(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'videos' + datePart + str(uuid.uuid4()) + extension
        return result

    class Meta:
        db_table = "videos"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    cover = ImageField(upload_to=get_upload_to_cover, null=True, default='videos_covers/default.jpg')
    video = models.FileField(upload_to=get_upload_to_video, null=True, default='videos/default.jpg')
    descr = models.TextField(blank=False, null=False)
    level = models.TextField(max_length=50, default=parameters.SUBSCRIPTION_LEVEL_FREE,
                             choices=parameters.SUBSCRIPTION_LEVEL_LIST,
                             null=False, blank=False)
    duration = models.IntegerField(default=0, blank=True, null=False)
    video_type = models.TextField(max_length=50, default=parameters.VIDEO_TYPE_COURSE,
                                  choices=parameters.VIDEO_TYPE_LIST,
                                  null=False, blank=False)
    instructor = models.ForeignKey(User, on_delete=models.CASCADE, default=None, null=False, blank=False)
    course = models.ForeignKey(Courses, on_delete=models.CASCADE, default=None, null=True, blank=True)
    content = models.ForeignKey(Content, on_delete=models.CASCADE, default=None, null=True, blank=True)
    age_access = models.CharField(max_length=20, choices=parameters.AGE_ACCESS_LIST,
                                  default=parameters.AGE_ACCESS_ALLOW)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


# These two auto-delete files from filesystem when they are unneeded:
@receiver(models.signals.post_delete, sender=Videos)
def auto_delete_video_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.cover:
        if os.path.isfile(instance.cover.path):
            os.remove(instance.cover.path)

    if instance.video:
        if os.path.isfile(instance.video.path):
            os.remove(instance.video.path)


@receiver(models.signals.pre_save, sender=Videos)
def auto_delete_video_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file_object = Videos.objects.get(pk=instance.pk)
        old_file_cover = old_file_object.cover
        old_file_video = old_file_object.video
    except Videos.DoesNotExist:
        return False

    new_file_cover = instance.cover
    if not old_file_cover == new_file_cover:
        if os.path.isfile(old_file_cover.path):
            os.remove(old_file_cover.path)

    new_file_video = instance.video
    if not old_file_video == new_file_video:
        if os.path.isfile(old_file_video.path):
            os.remove(old_file_video.path)


class Materials(Model):
    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'materials' + datePart + str(uuid.uuid4()) + extension
        return result

    class Meta:
        db_table = "materials"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    file = models.FileField(null=False, blank=False, upload_to=get_upload_to)
    ext = models.CharField(max_length=10, null=True, blank=True, default=None)
    mime = models.CharField(max_length=50, null=True, blank=True, default=None)
    size = models.BigIntegerField(default=0, null=False, blank=False)
    type = models.TextField(max_length=50, default=parameters.MATERIAL_TYPE_PDF,
                            choices=parameters.MATERIAL_TYPE_LIST,
                            null=False, blank=False)
    content = models.ForeignKey(Content, related_name='content_material', on_delete=models.CASCADE, default=None,
                                null=True, blank=True)
    course = models.ForeignKey(Courses, related_name='course_material', on_delete=models.CASCADE, default=None,
                               null=True, blank=True)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


@receiver(models.signals.post_delete, sender=Materials)
def auto_delete_materials_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)


@receiver(models.signals.pre_save, sender=Materials)
def auto_delete_materials_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file = Materials.objects.get(pk=instance.pk).file
    except Materials.DoesNotExist:
        return False

    new_file = instance.file
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class TranScripts(Model):
    class Meta:
        db_table = "transcripts"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    type = models.TextField(max_length=50, default=parameters.MATERIAL_TYPE_PDF,
                            choices=parameters.MATERIAL_TYPE_LIST,
                            null=False, blank=False)
    video = models.ForeignKey(Videos, related_name='video_transcript', on_delete=models.CASCADE, null=False,
                              blank=False)
    course = models.ForeignKey(Courses, related_name='course_transcript', on_delete=models.CASCADE, default=None,
                               null=True, blank=True)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class Subtitles(Model):
    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'subtitles' + datePart + str(uuid.uuid4()) + extension
        return result

    class Meta:
        db_table = "subtitles"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    file = models.FileField(null=False, blank=False, upload_to=get_upload_to)
    ext = models.CharField(max_length=10, null=True, blank=True, default=None)
    mime = models.CharField(max_length=50, null=True, blank=True, default=None)
    size = models.BigIntegerField(default=0, null=False, blank=False)
    lang = models.TextField(max_length=50, default=parameters.SUBTITLE_LANG_AR,
                            choices=parameters.SUBTITLE_LANG_LIST,
                            null=False, blank=False)
    video = models.ForeignKey(Videos, related_name='video_subtitle', on_delete=models.CASCADE, null=False, blank=False)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


@receiver(models.signals.post_delete, sender=Subtitles)
def auto_delete_subtitle_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)


@receiver(models.signals.pre_save, sender=Subtitles)
def auto_delete_subtitle_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file = Subtitles.objects.get(pk=instance.pk).file
    except Subtitles.DoesNotExist:
        return False

    new_file = instance.file
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class Paths(Model):
    def get_upload_to_cover(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'paths_covers' + datePart + str(uuid.uuid4()) + extension
        return result

    class Meta:
        db_table = "paths"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    cover = ImageField(upload_to=get_upload_to_cover, null=True, default='videos_covers/default.jpg')
    descr = models.TextField(blank=False, null=False)
    level = models.TextField(max_length=50, default=parameters.SUBSCRIPTION_LEVEL_FREE,
                             choices=parameters.SUBSCRIPTION_LEVEL_LIST,
                             null=False, blank=False)
    duration = models.IntegerField(default=0, blank=True, null=False)
    # Institution id if the path for certail institution
    institution = models.ForeignKey(Institutions, on_delete=models.CASCADE, blank=True, null=True, default=None)
    # Offer
    has_offer = models.BooleanField(default=False)
    offer_discount_perc = models.PositiveIntegerField(default=0)
    offer_start_date = models.DateField(default=None, null=True, blank=True)
    offer_end_date = models.DateField(default=None, null=True, blank=True)

    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


# These two auto-delete files from filesystem when they are unneeded:
@receiver(models.signals.post_delete, sender=Paths)
def auto_delete_paths_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.cover:
        if os.path.isfile(instance.cover.path):
            os.remove(instance.cover.path)


@receiver(models.signals.pre_save, sender=Paths)
def auto_delete_paths_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file_object = Paths.objects.get(pk=instance.pk)
        old_file_cover = old_file_object.cover
    except Paths.DoesNotExist:
        return False

    new_file_cover = instance.cover
    if not old_file_cover == new_file_cover:
        if os.path.isfile(old_file_cover.path):
            os.remove(old_file_cover.path)


class PathUsers(Model):
    class Meta:
        db_table = "paths_users"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False)
    # Institution id if the path for certail institution
    institution = models.ForeignKey(Institutions, on_delete=models.CASCADE, blank=True, null=True, default=None)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class PathGroups(Model):
    class Meta:
        db_table = "paths_groups"

    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(InstitutionGroup, on_delete=models.CASCADE, null=False, blank=False)
    # Institution id if the path for certail institution
    institution = models.ForeignKey(Institutions, on_delete=models.CASCADE, blank=True, null=True, default=None)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class PathCollection(Model):
    class Meta:
        db_table = "path_collections"

    id = models.BigAutoField(primary_key=True)
    path = models.ForeignKey(Paths, on_delete=models.CASCADE, null=False, blank=False)
    type = models.CharField(max_length=255, blank=False, null=False,
                            choices=parameters.TYPE_LIST,
                            default=parameters.TYPE_COURSE)
    type_id = models.BigIntegerField(null=False, blank=False)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


# PACKAGES
class Package(Model):
    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'packages' + datePart + str(uuid.uuid4()) + extension
        return result

    class Meta:
        db_table = "packages"

    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False, blank=False)
    img = ImageField(upload_to=get_upload_to, null=True, default='profiles/default_user.jpg')
    level = models.TextField(max_length=50, default=parameters.SUBSCRIPTION_LEVEL_FREE,
                             choices=parameters.SUBSCRIPTION_LEVEL_LIST,
                             null=False, blank=False)
    num_of_users = models.PositiveIntegerField(default=0)
    is_paid = models.BooleanField(default=True)
    is_public = models.BooleanField(default=True)
    duration = models.IntegerField(default=1)
    library = models.CharField(max_length=255, blank=False, null=False,
                               choices=parameters.LIBRARY_TYPE_LIST,
                               default=parameters.LIBRARY_TYPE_ALL)
    has_offer = models.BooleanField(default=False)
    offer_discount_perc = models.PositiveIntegerField(default=0)
    offer_start_date = models.DateField(default=None, null=True, blank=True)
    offer_end_date = models.DateField(default=None, null=True, blank=True)

    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


# These two auto-delete files from filesystem when they are unneeded:
@receiver(models.signals.post_delete, sender=Package)
def auto_delete_package_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.img:
        if os.path.isfile(instance.img.path):
            os.remove(instance.img.path)


@receiver(models.signals.pre_save, sender=Package)
def auto_delete_package_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file = Package.objects.get(pk=instance.pk).img
    except Package.DoesNotExist:
        return False

    new_file = instance.img
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class PackageCollection(Model):
    class Meta:
        db_table = "package_collections"

    id = models.BigAutoField(primary_key=True)
    package = models.ForeignKey(Package, on_delete=models.CASCADE, null=False, blank=False)
    type = models.CharField(max_length=255, blank=False, null=False,
                            choices=parameters.TYPE_LIST,
                            default=parameters.TYPE_COURSE)
    type_id = models.BigIntegerField(null=False, blank=False)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class Promotions(Model):
    class Meta:
        db_table = "promotions"

    id = models.BigAutoField(primary_key=True)
    code = models.CharField(max_length=255, unique=True, null=False, blank=True)
    run_times = models.TextField(max_length=50, default=parameters.PROMO_RUN_TIME,
                                 choices=parameters.PROMO_RUN_LIST,
                                 null=False, blank=False)
    target = models.TextField(max_length=50, default=parameters.PROMO_TARGET_ALL,
                              choices=parameters.PROMO_TARGET_LIST,
                              null=False, blank=False)
    expired = models.BooleanField(default=False)
    discount_perc = models.PositiveIntegerField(null=False)
    start_date = models.DateField(default=None, null=True, blank=True)
    end_date = models.DateField(default=None, null=True, blank=True)

    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


# User Data
class UserCourses(Model):
    class Meta:
        db_table = "user_courses"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    course = models.ForeignKey(Courses, blank=False, null=False, on_delete=models.CASCADE)
    content = models.ForeignKey(Content, blank=False, null=False, on_delete=models.CASCADE)
    start_date = models.DateField(default=None, null=True, blank=True)
    end_date = models.DateField(default=None, null=True, blank=True)

    num_of_contents = models.PositiveIntegerField(default=0, null=False, blank=True)
    completed_contents = models.PositiveIntegerField(default=0, null=False, blank=True)
    completed = models.BooleanField(default=False)

    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class UserPaths(Model):
    class Meta:
        db_table = "user_paths"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    path = models.ForeignKey(Paths, blank=False, null=False, on_delete=models.CASCADE)
    course = models.ForeignKey(Courses, blank=False, null=False, on_delete=models.CASCADE)
    content = models.ForeignKey(Content, blank=False, null=False, on_delete=models.CASCADE)
    start_date = models.DateField(default=None, null=True, blank=True)
    end_date = models.DateField(default=None, null=True, blank=True)
    num_of_courses = models.PositiveIntegerField(default=0, null=False, blank=True)
    completed_courses = models.PositiveIntegerField(default=0, null=False, blank=True)
    completed = models.BooleanField(default=False)

    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class UserVideos(Model):
    class Meta:
        db_table = "user_videos"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    video = models.ForeignKey(Videos, blank=False, null=False, on_delete=models.CASCADE)
    completed = models.BooleanField(default=False)
    start_date = models.DateField(default=None, null=True, blank=True)
    end_date = models.DateField(default=None, null=True, blank=True)

    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class UserBooks(Model):
    class Meta:
        db_table = "user_books"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    book = models.ForeignKey(BookSummery, blank=False, null=False, on_delete=models.CASCADE)
    start_date = models.DateField(default=None, null=True, blank=True)

    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class UserExams(Model):
    class Meta:
        db_table = "user_exams"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    exam = models.ForeignKey(Exam, blank=False, null=False, on_delete=models.CASCADE)
    start_time = models.DateTimeField(default=None, null=True, blank=True)
    end_time = models.DateTimeField(default=None, null=True, blank=True)
    finished = models.BooleanField(default=False)
    grade = models.PositiveIntegerField(default=0)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class UserAnswers(Model):
    class Meta:
        db_table = "user_answers"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    exam = models.ForeignKey(Exam, blank=False, null=False, on_delete=models.CASCADE)
    question = models.ForeignKey(Questions, blank=False, null=False, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answers, blank=False, null=False, on_delete=models.CASCADE)
    correct = models.NullBooleanField(default=None)
    start_time = models.DateTimeField(default=None, null=True, blank=True)
    end_time = models.DateTimeField(default=None, null=True, blank=True)
    finished = models.BooleanField(default=False)
    grade = models.PositiveIntegerField(default=0)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class UserCats(Model):
    class Meta:
        db_table = "user_cats"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, blank=False, null=False, on_delete=models.CASCADE)
    grade = models.PositiveIntegerField(default=0)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class UserWatched(Model):
    class Meta:
        db_table = "user_watched"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    Video = models.ForeignKey(Videos, default=None, blank=True, null=True, on_delete=models.CASCADE)
    book = models.ForeignKey(BookSummery, default=None, blank=True, null=True, on_delete=models.CASCADE)
    grade = models.PositiveIntegerField(default=0)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class UserLater(Model):
    class Meta:
        db_table = "user_later"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    Video = models.ForeignKey(Videos, default=None, blank=True, null=True, on_delete=models.CASCADE)
    book = models.ForeignKey(BookSummery, default=None, blank=True, null=True, on_delete=models.CASCADE)
    course = models.ForeignKey(Courses, default=None, blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class UserStatistics(Model):
    class Meta:
        db_table = "user_statistics"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    num_of_courses = models.PositiveIntegerField(default=0)
    num_of_books = models.PositiveIntegerField(default=0)
    num_of_videos = models.PositiveIntegerField(default=0)
    num_of_exams = models.PositiveIntegerField(default=0)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class UserPackage(Model):
    class Meta:
        db_table = "user_packages"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    package = models.ForeignKey(Package, blank=True, null=False, on_delete=models.CASCADE)
    start_date = models.TimeField(null=False, default=now)
    end_date = models.TimeField(null=False, default=now)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)


class UserPackagesHistory(Model):
    class Meta:
        db_table = "user_packages_history"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    package = models.ForeignKey(Package, blank=True, null=False, on_delete=models.CASCADE)
    start_date = models.TimeField(null=False, default=now)
    end_date = models.TimeField(null=False, default=now)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, null=True, blank=True)
