# Generated by Django 2.0.6 on 2018-09-03 18:35

import db.models
from django.db import migrations, models
import django.db.models.deletion
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0004_auto_20180903_1739'),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('image', sorl.thumbnail.fields.ImageField(default='authors/default.jpg', null=True, upload_to=db.models.Author.get_upload_to)),
                ('date_of_birth', models.DateField()),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('phone_number', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'author',
            },
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('descr', models.TextField()),
                ('cover', sorl.thumbnail.fields.ImageField(default='books_covers/default.jpg', null=True, upload_to=db.models.Book.get_upload_to_cover)),
                ('year', models.CharField(default='', max_length=255)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='db.Author')),
            ],
            options={
                'db_table': 'books',
            },
        ),
        migrations.RemoveField(
            model_name='booksummery',
            name='descr',
        ),
        migrations.RemoveField(
            model_name='booksummery',
            name='name',
        ),
        migrations.AddField(
            model_name='booksummery',
            name='video',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='db.Videos'),
            preserve_default=False,
        ),
        migrations.AlterModelTable(
            name='booksummery',
            table='book_summary',
        ),
        migrations.AddField(
            model_name='booksummery',
            name='book',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='db.Book'),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='book',
            unique_together={('name', 'author')},
        ),
    ]
